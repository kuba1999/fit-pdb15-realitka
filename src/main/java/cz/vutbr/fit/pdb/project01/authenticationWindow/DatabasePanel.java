
package cz.vutbr.fit.pdb.project01.authenticationWindow;

import cz.vutbr.fit.pdb.project01.repository.DatabaseRepository;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Jakub Švestka
 */
public class DatabasePanel extends JPanel implements KeyListener{
    
    private DatabaseRepository DatabaseRepository;
    
    JTextField connectionURL, username;
    JPasswordField password;
    
    /**
     * Constructor
     */
    public DatabasePanel() 
    {      
        this.DatabaseRepository = DatabaseRepository.GetConfiguration();
        placeComponents();
    }
    
    private void placeComponents()
    {
        this.setLayout(new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.anchor = GridBagConstraints.EAST;   
        
        JLabel userLabel = new JLabel("URL");
        add(userLabel, gbc);
        
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;   
        connectionURL = new JTextField(DatabaseRepository.DatabaseModel.getURL(), 30);
        connectionURL.addKeyListener(this);
        add(connectionURL, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;        
        gbc.anchor = GridBagConstraints.EAST;          
        JLabel usernameLabel = new JLabel("Uživatelské jméno");
        add(usernameLabel, gbc);
      
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST; 
        username = new JTextField(DatabaseRepository.DatabaseModel.getUsername(), 20);
        username.addKeyListener(this);
        add(username, gbc);
                
        gbc.gridx = 0;
        gbc.gridy = 2;        
        gbc.anchor = GridBagConstraints.EAST;   
        JLabel passwordLabel = new JLabel("Heslo");
        add(passwordLabel, gbc);
        
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST; 
        password = new JPasswordField(DatabaseRepository.DatabaseModel.getPassword(), 20);
        password.addKeyListener(this);
        add(password, gbc);        
    }     

    @Override
    public void keyTyped(KeyEvent e) { }

    @Override
    public void keyPressed(KeyEvent e) { }

    @Override
    /**
     * Ulozeni zadanych udaju z formulare
     */
    public void keyReleased(KeyEvent e) {
        DatabaseRepository.DatabaseModel.setURL(connectionURL.getText());
        DatabaseRepository.DatabaseModel.setUsername(username.getText());
        DatabaseRepository.DatabaseModel.setPassword(new String(password.getPassword())); 
    }
}
