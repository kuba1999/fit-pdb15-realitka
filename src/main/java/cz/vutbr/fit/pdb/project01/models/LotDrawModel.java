package cz.vutbr.fit.pdb.project01.models;


import java.awt.Color;
import java.awt.Point;
/**
 *
 * @author Jakub Švestka
 */
public class LotDrawModel extends PolygonDrawModel implements IOffer{
    
    private String name;
    private Integer area = 0;
    private Integer locality;
    private String address;
    private String description;
    private String user;

    /**
     * Constructor
     * @param point 
     */
    public LotDrawModel(Point point) 
    {
        super(point);
        
        setFillColor(Color.RED);
        setStrokeColor(Color.BLACK);
    }
    
    /**
     * Get name
     * @return 
     */
    public String getName()
    {
        return name;
    }   
    
    /**
     * Set name
     * @param name 
     */
    public void setName(String name)
    {
        this.name = name;
    }  

    /**
     * Get area
     * @return 
     */
    public Integer getArea() {
        return area;
    }

    /**
     * Set area
     * @param area 
     */
    public void setArea(Integer area) {
        this.area = area;
    }

    /**
     * Get locality
     * @return 
     */
    public Integer getLocality() {
        return locality;
    }

    /**
     * Set locality
     * @param locality 
     */
    public void setLocality(Integer locality) {
        this.locality = locality;
    }

    /**
     * Geet address
     * @return 
     */
    public String getAddress() {
        return address;
    }

    /**
     * Set address
     * @param address 
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Get description
     * @return 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set description
     * @param description 
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * Get user
     * @return 
     */
    public String getUser() {
        return user;
    }

    /**
     * Set user
     * @param user 
     */
    public void setUser(String user) {
        this.user = user;
    } 
    
    
    /**
     * Get type
     * @return 
     */
    public String getType() 
    {
        return "";
    }    
}
