/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import cz.vutbr.fit.pdb.project01.models.IOffer;
import cz.vutbr.fit.pdb.project01.models.PhotoModel;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * @author      Karolína Hajná      <xhajna00@stud.fit.vutbr.cz>
 * @version     1.0                 
 * @since       2015-11-28          
 */
public class ResultsPanel extends JPanel{
    
    /**
    * constraints for placing GUI components
    */
    GridBagConstraints panelgbc;
    /**
    * image scaled to calculated widht and height
    */
    int gridy = 0;
    /**
    * font for offer name
    */
    Font nameFont = new Font(null, Font.BOLD, 22);
    /**
    * font for other offer data
    */
    Font locAreaFont = new Font (null, Font.PLAIN, 16);
    /**
    * main application window
    */
    AppWindow mainWindow;
    /**
    * panel containing scrollpanel with this panel
    */
    JPanel topPanel;
    
    /**
    * Initializes window appearance
    * 
    * @param  tp tab holding scrollbar with this panel
    * @param win main application window
    */
    public ResultsPanel(AppWindow win, JPanel tp){
        mainWindow = win;
        topPanel = tp;
        
        setLayout(new GridBagLayout());
        panelgbc = new GridBagConstraints();
        panelgbc.fill = GridBagConstraints.HORIZONTAL;
        panelgbc.insets = new Insets(5, 5, 5, 5);
        panelgbc.anchor = GridBagConstraints.NORTHWEST;
        panelgbc.weightx = 1.0;
        panelgbc.weighty = 0.0;
        
        panelgbc.gridy = 1000;
        JButton button = new JButton("Zavřít");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mainWindow.removePanel(topPanel);
            }
        });
        add(button, panelgbc);
    }
   
    /**
    * add new entry to results and show it in panel
    * 
     * @param entity
    */
    public void addResult(final BaseDrawModel entity){
        
        BufferedImage image = null;
        List<PhotoModel> pms = PhotoModel.findByOfferId(entity.getId());
        if (!pms.isEmpty()) {
            image = pms.get(0).getPhoto();
        }
       
        JPanel result = new JPanel();
        result.setLayout(new GridBagLayout());
        GridBagConstraints resultgbc = new GridBagConstraints();
        resultgbc.insets = new Insets(5, 5, 5, 5);
        resultgbc.anchor = GridBagConstraints.WEST;
        resultgbc.weightx = 0.0;
        resultgbc.weighty = 1.0;
        
        resultgbc.gridx = 0;
        resultgbc.gridy = 0;
        resultgbc.gridheight = 3;
        if(image != null)
        {
            int imHeight = image.getHeight();
            int imWidth = image.getWidth();
            double newWidth = imWidth*(200/(double)imHeight);
            Image scaled_image = image.getScaledInstance((int) newWidth, 200, Image.SCALE_DEFAULT);	
            result.add(new JLabel(new ImageIcon(scaled_image)), resultgbc);
        }

        resultgbc.gridheight = 1;
        resultgbc.gridx = 1;
        JLabel nameLabel = new JLabel(((IOffer)entity).getName());
        nameLabel.setFont(nameFont);
        result.add(nameLabel, resultgbc);
        
        resultgbc.gridy = 1;
        JLabel locationLabel = new JLabel(localityConverter(((IOffer)entity).getLocality()));
        locationLabel.setFont(locAreaFont);
        result.add(locationLabel, resultgbc);

        resultgbc.gridy = 2;
        JLabel areaLabel = new JLabel(Integer.toString(((IOffer)entity).getArea()) + " m2");
        areaLabel.setFont(locAreaFont);
        result.add(areaLabel, resultgbc);
        
        resultgbc.gridx = 2;
        resultgbc.gridy = 2;
        resultgbc.weightx = 1.0;
        resultgbc.anchor = GridBagConstraints.EAST;
        JButton openResult = new JButton("Zobrazit");
        openResult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    mainWindow.addTab(new OfferDetailsPanel(entity, mainWindow), "Detail Nabídky");
                } catch (IOException ex) {
                    Logger.getLogger(ResultsPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
;            }
        });
        result.add(openResult, resultgbc);
        
        panelgbc.gridx = 0;
        panelgbc.gridy = gridy;
        add(result, panelgbc);
        gridy++;
    }
    
    /**
    * fill bottom of panel so conponents are aligned ao top without additional spaces between them
    */
    public void addFiller(){
        panelgbc.weighty = 1.0;
        add(new JLabel(), panelgbc);
    }

    /**
     * Convert locality to string representation
     * @param l
     * @return 
     */
    public static String localityConverter(Integer l)
    {
        switch(l)
        {
            case 0: return "Brno - sever";
            case 1: return "Brno - střed";
            case 2: return "Černovice";
            case 3: return "Jundrov";
            case 4: return "Komín";
            case 5: return "Královo Pole";
            case 6: return "Maloměřice a Obřany";
            case 7: return "Medlánky";
            case 8: return "Nový Lískovec";
            case 9: return "Vinohrady";
            case 10: return "Žabovřesky";
            case 11: return "Židenice";
            default: return "";
        }
    }    
}
