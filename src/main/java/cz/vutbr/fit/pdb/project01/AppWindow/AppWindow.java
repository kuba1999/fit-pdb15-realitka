/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.AppWindow.SearchPanel;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import java.io.IOException;
import javax.swing.JScrollPane;

/**
 * @author      Karolína Hajná      <xhajna00@stud.fit.vutbr.cz>
 * @version     1.0                 
 * @since       2015-10-19          
 */
public class AppWindow extends JFrame{
    /**
    * Panel with tabs in main application window
    */
    private JTabbedPane tabsPanel;
    /**
    * Tab used for search in DB
    */
    private JPanel searchPanel;
    private ResultsPanel resultsPanel;
    /**
    * Login of currently logged in user
    */
    private String loggedUser;
    
    private JPanel topResultsPanel;

    /**
    * Initializes window appearance
    * 
    * @param  loggedUser currently logged in user name
     * @throws java.io.IOException
    */
    public AppWindow(String loggedUser) throws IOException{
        this.setSize(1280, 1024);
        this.setName("Realitni Kancelar");
        setResizable(true);
        this.loggedUser = loggedUser;
        
        JPanel topPanel = new JPanel();
        topPanel.setLayout( new BorderLayout() );
	getContentPane().add(topPanel);

        // Create a tabbed pane
        tabsPanel = new JTabbedPane();
        topPanel.add(tabsPanel, BorderLayout.CENTER);
        
        tabsPanel.addTab("Vyhledávání", new SearchPanel(this));
        tabsPanel.addTab("Úprava Mapy", new EditorMap(this));
        
        topResultsPanel = new JPanel(new BorderLayout());
    }
    
    /**
     * Add tab into tabs panel
     * 
     * @param tab tab object given to add to panel
     * @param name title of tab in panel
     */
    public void addTab(OfferDetailsPanel tab, String name){
        tabsPanel.addTab(name, tab);
    }
    
    
    /**
     * Get logged user
     * 
     * @return 
     */
    public String getLoggedUser()
    {
        return loggedUser;
    }
    
    /**
     * Closes given tab
     * 
     * @param panel tab object which needs to be closed
     */
    public void removePanel(OfferDetailsPanel panel){
        tabsPanel.remove(panel);
    }
    
    /**
     * Closes given tab
     * 
     * @param panel tab object which needs to be closed
     */
    public void removePanel(JPanel panel){
        tabsPanel.remove(panel);
    }
    
    
    /**
     * Remove panel with results
     */
    public void removePanelWithResults(){
        tabsPanel.remove(topResultsPanel);
    }
    
    /**
     * Show panel with results
     * @return 
     */
    public ResultsPanel showPanelWithResults(){
        topResultsPanel = new JPanel(new BorderLayout());
        resultsPanel = new ResultsPanel(this, topResultsPanel);
        topResultsPanel.add(new JScrollPane(resultsPanel));
        tabsPanel.addTab("Výsledky", topResultsPanel);
        
        return resultsPanel;
    }
}
