package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import cz.vutbr.fit.pdb.project01.models.BuStopDrawModel;
import cz.vutbr.fit.pdb.project01.models.BuildingDrawModel;
import cz.vutbr.fit.pdb.project01.models.LotDrawModel;
import cz.vutbr.fit.pdb.project01.models.NoisyZoneDrawModel;
import cz.vutbr.fit.pdb.project01.models.PointDrawModel;
import cz.vutbr.fit.pdb.project01.models.PolygonDrawModel;
import cz.vutbr.fit.pdb.project01.models.RoadDrawModel;
import cz.vutbr.fit.pdb.project01.models.SchoolDrawModel;
import cz.vutbr.fit.pdb.project01.models.WifiDrawModel;
import cz.vutbr.fit.pdb.project01.repository.BuildingsDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.BusStopDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.LotDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.NoisyZoneDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.RoadDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.SchoolDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.WifiDrawRepository;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.PathIterator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

 
/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class EditDrawArea extends BaseDrawArea{
    
    private final List<BaseDrawModel> selectedEntities = new ArrayList<>();
    
    boolean selectedEntityMoved = false;
    
    
    /**
     * Constructor
     * 
     * @param editorMap
     */
    public EditDrawArea(EditorMap editorMap) 
    {
        this.editorMap = editorMap;

               
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                try {
                    entityMouseClicked(evt);
                } catch (IOException ex) {
                    Logger.getLogger(EditDrawArea.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            public void mouseEntered(MouseEvent evt) {
                //formMouseEntered(evt);
            }
            public void mouseExited(MouseEvent evt) {
                //formMouseExited(evt);
            }
            public void mousePressed(MouseEvent evt) {
                entityMousePressed(evt);
                
            }
            public void mouseReleased(MouseEvent evt) {
                entityMouseReleased(evt.getPoint());
            }
        });
        
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) 
            {
                entityMouseDragged(evt.getPoint());

            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                entityMouseMoved(evt);
            }
        });


        
        
        //load entities
        loadEntitiesFromDB();
        loadOffersFromDB();
        
        //Bind event for removing entities
        editorMap.clearBtn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    removeSelectedEntities();
                }     
             }); 
        
        
        bindFilterCheckboxes();
    }


    
    /**
     * Changing positiion of entity
     * @param position 
     */
    private void entityMouseDragged(Point position)
    {          
        //creating new entity
        
        //Wifi
        if(drawedEntity != null && drawedEntity instanceof WifiDrawModel)
        {
            ((WifiDrawModel) drawedEntity).calculateAndSetSize(position);
            repaint();
            
            return;
        }
        
        
        
        if(selectedEntity != null)
        {
            //we do not paint outside map
            if(selectedEntity instanceof PointDrawModel)
            {
                if((position.getX()+20) > (int)MapDimension.getWidth() || 
                        (position.getY()+20) > (int)MapDimension.getHeight() || 
                        (position.getX()-20) < 0 || 
                        (position.getY()-20) < 0)
                {
                    return;
                }                
            }
            else if(selectedEntity instanceof WifiDrawModel)
            {
                WifiDrawModel wifi = (WifiDrawModel) selectedEntity;
                
                if((position.getX()+wifi.getRadius()) > (int)MapDimension.getWidth() || 
                        (position.getY()+wifi.getRadius()) > (int)MapDimension.getHeight() || 
                        (position.getX()-wifi.getRadius()) < 0 || 
                        (position.getY()-wifi.getRadius()) < 0)
                {
                    return;
                }                  
            }
            
            //is the position changed?
            if(position.x != selectedEntity.getX() || position.y != selectedEntity.getY() )
            {
                selectedEntityMoved = true;

                //Change position
                selectedEntity.move(position);
                repaint();                
            }
        }        
    }
    
    /**
     * Actions when mouse relased 
     * @param position 
     */
    private void entityMouseReleased(Point position)
    {          
        //creating new entity
        if(drawedEntity != null)
        {
            if(drawedEntity instanceof WifiDrawModel)
            {
                String values[] = showWiFiDialog();
                
                WifiDrawModel e = (WifiDrawModel) drawedEntity;
                
                e.setSSID(values[0]);
                e.setChannel(Integer.parseInt(values[1]));
                
                new WifiDrawRepository().insert(e);
            }
            
            if(drawedEntity instanceof RoadDrawModel || drawedEntity instanceof NoisyZoneDrawModel || drawedEntity instanceof LotDrawModel)
            {
                return;
            }
                
            
            
            drawedEntity = null;
            
            //repaint all
            repaint();
            
            return;
        }


        //Moving with entity
        if(selectedEntity != null)
        {
            if(selectedEntityMoved)
            {
                //Update position
                if(selectedEntity instanceof SchoolDrawModel)
                {
                    (new SchoolDrawRepository()).updatePosition((SchoolDrawModel)selectedEntity);     
                }
                else if(selectedEntity instanceof BuStopDrawModel)
                {
                    (new BusStopDrawRepository()).updatePosition((BuStopDrawModel)selectedEntity);     
                }
                else if(selectedEntity instanceof WifiDrawModel)
                {
                    (new WifiDrawRepository()).updatePosition((WifiDrawModel)selectedEntity);     
                }
                else if(selectedEntity instanceof RoadDrawModel)
                {
                    ((RoadDrawModel)selectedEntity).clearClickedPoint();
                    (new RoadDrawRepository()).updatePosition((RoadDrawModel)selectedEntity);     
                }
                else if(selectedEntity instanceof NoisyZoneDrawModel)
                {
                    ((PolygonDrawModel)selectedEntity).clearClickedPoint();
                    (new NoisyZoneDrawRepository()).updatePosition((NoisyZoneDrawModel)selectedEntity);     
                }
                else if(selectedEntity instanceof BuildingDrawModel)
                {
                    (new BuildingsDrawRepository()).updatePosition((BuildingDrawModel)selectedEntity);     
                }
                else if(selectedEntity instanceof LotDrawModel)
                {
                    ((PolygonDrawModel)selectedEntity).clearClickedPoint();
                    (new LotDrawRepository()).updatePosition((LotDrawModel)selectedEntity);  
                }
                
                //reset
                selectedEntityMoved = false;
            }

            
            //deselect entity
            if(selectedEntities.contains(selectedEntity) == false) //deselect only when entity is not contained in selection list
            {
                selectedEntity.setSelected(false); //deselect
            }
            selectedEntity = null; 
            repaint();
        }
    }
    
    /**
     * Actions when mouse pressed
     * @param position 
     */
    private void entityMousePressed(MouseEvent evt)
    {
        //create new enitity

        if(editorMap.getSelectedTypeOfEntity() == EditorMap.typeOfentities.WIFI)
        {      
                WifiDrawModel entity = new WifiDrawModel(evt.getPoint(), 20);
                drawedEntity = entity;
                
                entities.add(entity);
                repaint();
        }     
        
        if(drawedEntity != null)
            return;

        //select existing entity 
        BaseDrawModel entity = getHitEntity(evt.getPoint());
        
        if(entity == null)
            return;

        if(selectedEntity != null && selectedEntity == entity)
            return;

        selectedEntity = entity;
        entity.setSelected(); 
        repaint();
  
    }
    
    /**
     * Actions when mouse clicked
     * @param position 
     */
    private void entityMouseClicked(MouseEvent evt) throws IOException
    {        
        //handle double click event for editing entity
        if (evt.getClickCount() == 2 && !evt.isConsumed() && evt.getButton() == 1 && drawedEntity == null) {
             evt.consume();
             //handle double click event.
             
             BaseDrawModel hitEntity = getHitEntity(evt.getPoint());
             
             if(hitEntity instanceof SchoolDrawModel)
             {
                String name = (String)JOptionPane.showInputDialog(null, 
                        "Název školy:", "Škola", JOptionPane.INFORMATION_MESSAGE, null, null, ((SchoolDrawModel)hitEntity).getName());
                
                if(name != null)
                {
                    ((SchoolDrawModel)hitEntity).setName(name);
                    (new SchoolDrawRepository()).update((SchoolDrawModel)hitEntity);
                }
             } 
             else if(hitEntity instanceof BuStopDrawModel)
             {
                String name = (String)JOptionPane.showInputDialog(null, 
                        "Název zastávky:", "MHD", JOptionPane.INFORMATION_MESSAGE, null, null, ((BuStopDrawModel)hitEntity).getName());
                
                if(name != null)
                {
                    ((BuStopDrawModel)hitEntity).setName(name);
                    (new BusStopDrawRepository()).update((BuStopDrawModel)hitEntity);
                }
             }
             else if(hitEntity instanceof WifiDrawModel)
             {
                WifiDrawModel e = (WifiDrawModel) hitEntity;
                 
                String values[] = showWiFiDialog(e.getSSID(), e.getChannel());

                e.setSSID(values[0]);
                e.setChannel(Integer.parseInt(values[1]));
                
                (new WifiDrawRepository()).update(e);
                repaint();
             } 
             else if(hitEntity instanceof NoisyZoneDrawModel)
             {
                    String noise = (String)JOptionPane.showInputDialog(null, 
                        "Úroveň hlučnosti:", "MHD", JOptionPane.INFORMATION_MESSAGE, null, null, ((NoisyZoneDrawModel)hitEntity).getNoise());
                
                if(noise != null)
                {
                    try {
                      ((NoisyZoneDrawModel)hitEntity).setNoise(Integer.parseInt(noise));
                    } catch (NumberFormatException e) {
                      ((NoisyZoneDrawModel)hitEntity).setNoise(0);
                    }                    
                    (new NoisyZoneDrawRepository()).update((NoisyZoneDrawModel)hitEntity);
                }
             }  
             else if(hitEntity instanceof BuildingDrawModel)
             {
                BuildingDrawModel entity = (BuildingDrawModel) hitEntity;

                //show form for entering details about house/flat
                OfferForm form = new OfferForm(entity);
                form.setDefaultValues(entity.getName(), entity.getType(), entity.getArea(), entity.getLocality(), entity.getAddress(), entity.getDescription());
                form.setLocationRelativeTo(editorMap);
                form.validateForm();

                if(form.isFormValid())
                {
                    (new BuildingsDrawRepository()).update(entity);
                    form.setVisible(true);
                    ImagesDialog imDialog = new ImagesDialog(entity);
                    imDialog.setVisible(true);
                    repaint();
                } else {
                    Debugging.error("Invalid form.");
                }
             }  
             else if(hitEntity instanceof LotDrawModel)
             {
                LotDrawModel entity = (LotDrawModel) hitEntity;

                //show form for entering details about house/flat
                OfferForm form = new OfferForm(entity);
                form.setDefaultValues(entity.getName(), entity.getArea(), entity.getLocality(), entity.getAddress(), entity.getDescription());
                form.setLocationRelativeTo(editorMap);
                form.setVisible(true);


                if(form.isFormValid())
                {
                    (new LotDrawRepository()).update(entity);
                    repaint();
                }
             }  
        }
        
        
        //handle right click for selecting entity
        if (evt.getButton() == 3)
        {
            if(drawedEntity != null && drawedEntity instanceof RoadDrawModel) //end of drawing
            {
                RoadDrawModel entity = ((RoadDrawModel) drawedEntity);
                
                entity.removeFakePoint();
                
                //Save to DB
                if(entity.countPoints() > 1) //Insert line to DB if we have segment of line
                {
                    Debugging.info("We can save line to DB");
                    (new RoadDrawRepository()).insert(entity);
                }
                else
                {
                    entities.remove(entity);
                }
                
                repaint();
                drawedEntity = null;
                return;
            }
            
            if(drawedEntity != null && drawedEntity instanceof NoisyZoneDrawModel) //end of drawing
            {
                NoisyZoneDrawModel entity = ((NoisyZoneDrawModel) drawedEntity);
                
                entity.removeFakePoint();
                
                repaint();
                
                //Save to DB
                if(entity.countPoints() > 2) //Insert line to DB if we have segment of line
                {
                    Debugging.info("We can save line to DB");
                    String noise = (String)JOptionPane.showInputDialog(null, 
                        "Úroveň hlučnosti:", "MHD", JOptionPane.INFORMATION_MESSAGE, null, null, "");
                    
                    
                    try {
                      entity.setNoise(Integer.parseInt(noise));
                    } catch (NumberFormatException e) {
                      entity.setNoise(0);
                    }
                    
                    (new NoisyZoneDrawRepository()).insert(entity);
                }
                else
                {
                    entities.remove(entity);
                }
                
                
                drawedEntity = null;
                return;
            }
            
            if(drawedEntity != null && drawedEntity instanceof LotDrawModel) //end of drawing
            {
                LotDrawModel entity = ((LotDrawModel) drawedEntity);
                
                entity.removeFakePoint();
                
                repaint();
                
                //Save to DB
                if(entity.countPoints() > 2) //Insert line to DB if we have segment of line
                {
                    Debugging.info("We can save line to DB");


                    //show form for entering details about house/flat
                    OfferForm form = new OfferForm(entity);
                    form.setLocationRelativeTo(editorMap);
                    form.setVisible(true);

                    if(form.isFormValid())
                    {
                        entity.setUser(editorMap.getMainFrame().getLoggedUser());
                        (new LotDrawRepository()).insert(entity); //save to DB
                        ImagesDialog imDialog = new ImagesDialog(entity);
                        imDialog.setLocationRelativeTo(editorMap);
                        imDialog.setVisible(true);
                        repaint();
                    }
                    else
                    {
                        //remove from map
                       entities.remove(entity);
                       repaint();               
                    }                    
                }
                else
                {
                    entities.remove(entity);
                }
                
                
                drawedEntity = null;
                return;
            }
            
            if(drawedEntity != null)
                return;
            
            BaseDrawModel e = getHitEntity(evt.getPoint());
            
            if(e == null)
                return;
            
            if(selectedEntities.contains(e))
            {
                e.setSelected(false);  //deselect and remove from list
                selectedEntities.remove(e);
            }
            else
            {
                e.setSelected();
                selectedEntities.add(e);
            }
            
            return;
        }
        

        //Create new entity
        if(editorMap.getSelectedTypeOfEntity() == EditorMap.typeOfentities.SCHOOL)
        {
            String name = JOptionPane.showInputDialog(null, "Název školy:", "Škola", JOptionPane.INFORMATION_MESSAGE);

            if(name != null)
            {         
                SchoolDrawModel entity = new SchoolDrawModel(evt.getPoint(), name);

                (new SchoolDrawRepository()).insert(entity); 

                entities.add(entity);
                repaint();
            }
        }
        else if(editorMap.getSelectedTypeOfEntity() == EditorMap.typeOfentities.BUS_STOP)
        {
            String name = JOptionPane.showInputDialog(null, "Název zastávky:", "MHD", JOptionPane.INFORMATION_MESSAGE);

            if(name != null)
            {         
                BuStopDrawModel entity = new BuStopDrawModel(evt.getPoint(), name);

                (new BusStopDrawRepository()).insert(entity); 

                entities.add(entity);
                repaint();
            }
        }
        else if(editorMap.getSelectedTypeOfEntity() == EditorMap.typeOfentities.ROAD)
        {
            //add new point to drawen entity
            if(drawedEntity != null && drawedEntity instanceof RoadDrawModel)
            {
                RoadDrawModel entity = (RoadDrawModel) drawedEntity;
                
                Debugging.info("next point");
                
                //add point
                entity.addPoint(evt.getPoint());
            }
            else
            {
                RoadDrawModel entity = new RoadDrawModel(evt.getPoint());

                drawedEntity = entity;  
                
                entities.add(entity);
            }
            
            repaint();
        }
        else if(editorMap.getSelectedTypeOfEntity() == EditorMap.typeOfentities.NOISE_ZONE)
        {
            //add new point to drawen entity
            if(drawedEntity != null && drawedEntity instanceof NoisyZoneDrawModel)
            {
                NoisyZoneDrawModel entity = (NoisyZoneDrawModel) drawedEntity;
                
                Debugging.info("next point");
                
                //add point
                entity.addPoint(evt.getPoint());
            }
            else
            {
                NoisyZoneDrawModel entity = new NoisyZoneDrawModel(evt.getPoint());

                drawedEntity = entity;  
                
                entities.add(entity);
            }
            
            repaint();
        }
        else if(editorMap.getSelectedTypeOfEntity() == EditorMap.typeOfentities.BUILDINGDS)
        {
            BuildingDrawModel entity = new BuildingDrawModel(evt.getPoint());
            
            //show on map
            entities.add(entity);
            repaint();
            
            //show form for entering details about house/flat
            OfferForm form = new OfferForm(entity);
            form.setLocationRelativeTo(editorMap);
            form.setVisible(true);
            
            if(form.isFormValid())
            {
                entity.setUser(editorMap.getMainFrame().getLoggedUser());
                (new BuildingsDrawRepository()).insert(entity); //save to DB
                ImagesDialog imDialog = new ImagesDialog(entity);
                imDialog.setLocationRelativeTo(editorMap);
                imDialog.setVisible(true);
                repaint();
            }
            else
            {
                //remove from map
               entities.remove(entity);
               repaint();               
            }
        }
        else if(editorMap.getSelectedTypeOfEntity() == EditorMap.typeOfentities.LOT)
        {
            //add new point to drawen entity
            if(drawedEntity != null && drawedEntity instanceof LotDrawModel)
            {
                LotDrawModel entity = (LotDrawModel) drawedEntity;
                
                Debugging.info("next point");
                
                //add point
                entity.addPoint(evt.getPoint());
            }
            else
            {
                LotDrawModel entity = new LotDrawModel(evt.getPoint());

                drawedEntity = entity;  
                
                entities.add(entity);
            }
            
            repaint();
        }
    }
    
    
    private void entityMouseMoved(MouseEvent evt)
    {
        if(drawedEntity instanceof RoadDrawModel)
        {
            RoadDrawModel entity = (RoadDrawModel) drawedEntity;
            
            entity.addFakePoint(evt.getPoint());
            repaint();
        } 
        else if(drawedEntity instanceof NoisyZoneDrawModel)
        {
            NoisyZoneDrawModel entity = (NoisyZoneDrawModel) drawedEntity;
            
            entity.addFakePoint(evt.getPoint());
            repaint();
        }  
        else if(drawedEntity instanceof LotDrawModel)
        {
            LotDrawModel entity = (LotDrawModel) drawedEntity;
            
            entity.addFakePoint(evt.getPoint());
            repaint();
        } 
    }

    
    /**
     * Remove selected entities
     */
    private void removeSelectedEntities()
    {
        for (Iterator<BaseDrawModel> iterator = selectedEntities.iterator(); iterator.hasNext(); ) 
        {
            BaseDrawModel entity = iterator.next();
            
            if(entity instanceof SchoolDrawModel)
            {
                (new SchoolDrawRepository()).remove((SchoolDrawModel)entity);
            }
            else if(entity instanceof BuStopDrawModel)
            {
                (new BusStopDrawRepository()).remove((BuStopDrawModel)entity);
            }
            else if(entity instanceof RoadDrawModel)
            {
                (new RoadDrawRepository()).remove((RoadDrawModel)entity);
            }
            else if(entity instanceof WifiDrawModel)
            {
                (new WifiDrawRepository()).remove((WifiDrawModel)entity);
            }
            else if(entity instanceof NoisyZoneDrawModel)
            {
                (new NoisyZoneDrawRepository()).remove((NoisyZoneDrawModel)entity);
            }
            else if(entity instanceof BuildingDrawModel)
            {
                (new BuildingsDrawRepository()).remove((BuildingDrawModel)entity);
            }
            
            else if(entity instanceof LotDrawModel)
            {
                (new LotDrawRepository()).remove((LotDrawModel)entity);
            }
            
            iterator.remove(); //remove from selected entities
            
            entities.remove(entity); //remove from entities which are displayed on map
        }
        
        repaint();
    }
    
    /**
     * Dialog for entering values for WiFi area
     * @param defaultSSID
     * @param defaultChannel
     * @return array{ssid, channel}
     */
    private String[] showWiFiDialog(String defaultSSID, Integer defaultChannel) 
    {
        Integer[] channelsList = {1,2,3,4,5,6,7,8,9,10,11,12,13};
        
        JPanel p = new JPanel(new BorderLayout(5,5));

        JPanel labels = new JPanel(new GridLayout(0,1,2,2));
        labels.add(new JLabel("SSID", SwingConstants.RIGHT));
        labels.add(new JLabel("Kanál", SwingConstants.RIGHT));
        p.add(labels, BorderLayout.WEST);

        JPanel controls = new JPanel(new GridLayout(0,1,2,2));
        JTextField ssid = new JTextField(defaultSSID);
        controls.add(ssid);
        JComboBox channel = new JComboBox(channelsList);
        channel.setSelectedItem(defaultChannel);
        controls.add(channel);
        p.add(controls, BorderLayout.CENTER);
        
        JOptionPane.showMessageDialog(
            editorMap, p, "WiFi", JOptionPane.QUESTION_MESSAGE);
        
        
        return new String[]{ ssid.getText(), channel.getSelectedItem().toString()};
    }
    
    /**
     * Dialog for entering values for WiFi area
     * @return array{ssid, channel}
     */    
    private String[] showWiFiDialog() 
    {
        return showWiFiDialog("", 1);
    }

    private void bindFilterCheckboxes() 
    {
         editorMap.offersChk.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) 
            {
              repaint();
            }         
         });
         editorMap.wifiChk.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) 
            {
              repaint();

            }         
         });
         editorMap.roadsChk.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) 
            {
              repaint();
            }         
         });
         editorMap.noiseChk.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) 
            {
              repaint();
            }         
         });   
         editorMap.schoolChk.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) 
            {
              repaint();
            }         
         });
         editorMap.busChk.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) 
            {
              repaint();
            }         
         });            
    }
    
    
    
}
