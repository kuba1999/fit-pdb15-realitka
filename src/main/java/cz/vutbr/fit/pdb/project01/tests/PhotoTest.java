/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.tests;

import cz.vutbr.fit.pdb.project01.Database;
import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.PhotoModel;
import cz.vutbr.fit.pdb.project01.repository.DatabaseRepository;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 * Photo model test and usage examples
 * @author Ondrej Koblizek
 */
public class PhotoTest {
    public PhotoTest() throws FileNotFoundException, IOException {
        DatabaseRepository dbRepository = DatabaseRepository.GetConfiguration();
        Database db = Database.getDatabase(dbRepository.DatabaseModel);
        db.initDatabase();

        Debugging.warning("PhotoTest: start");
        // get image data
        File file = new File("img/domek1.jpg");
        if (! file.exists()) {
            Debugging.error("PhotoTest: file not exists.");
            return;
        }

        // insert 1
        PhotoModel p = new PhotoModel();
        p.setPhoto(ImageIO.read(file));
        p.setMiniWidth(200);
        p.setMiniHeight(200);
        p.setTitle("Rodinny domek");
        p.save();
        p.delete();
        Debugging.info("photo 1: \n" + p);

        // find photo with id 1
        PhotoModel find = PhotoModel.findById(1);
        Debugging.info("find 1: \n" + find);

        // mirrorV
        find.mirrorV();

        // update existing photo
        find.setTitle("Domecek");
        find.save();
        Debugging.info("find update save: \n" + find);

        // How to display
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);

        JFrame editorFrame = new JFrame("PhotoTest - main image");
        editorFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

        BufferedImage image = null;
        try
        {
            image = find.getPhoto();
        }
        catch (Exception e)
        {
          e.printStackTrace();
          return;
        }
        ImageIcon imageIcon = new ImageIcon(image);
        JLabel jLabel = new JLabel();
        jLabel.setIcon(imageIcon);
        editorFrame.getContentPane().add(jLabel, BorderLayout.CENTER);

        editorFrame.pack();
        Point newLocation = new Point(middle.x - (editorFrame.getWidth()),
                                      middle.y - (editorFrame.getHeight() / 2));
        //editorFrame.setLocationRelativeTo(null);
        editorFrame.setLocation(newLocation);
        editorFrame.setVisible(true);

        // Mini image
        JFrame editorFrame2 = new JFrame("PhotoTest - mini image");
        editorFrame2.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

        try
        {
            image = find.getMini();
        }
        catch (Exception e)
        {
          e.printStackTrace();
          return;
        }
        ImageIcon imageIcon2 = new ImageIcon(image);
        JLabel jLabel2 = new JLabel();
        jLabel2.setIcon(imageIcon2);
        editorFrame2.getContentPane().add(jLabel2, BorderLayout.CENTER);

        editorFrame2.pack();
        newLocation = new Point(middle.x + 2 * (editorFrame2.getWidth()),
                                middle.y - (editorFrame2.getHeight() / 2));
        //editorFrame2.setLocationRelativeTo(null);
        editorFrame2.setLocation(newLocation);
        editorFrame2.setVisible(true);

        Debugging.warning("PhotoTest: finish");
    }
}
