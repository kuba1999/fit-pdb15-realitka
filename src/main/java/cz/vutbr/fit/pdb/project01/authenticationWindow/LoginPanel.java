package cz.vutbr.fit.pdb.project01.authenticationWindow;

import cz.vutbr.fit.pdb.project01.Database;
import cz.vutbr.fit.pdb.project01.EstateAgency;
import cz.vutbr.fit.pdb.project01.controllers.AuthenticationController;
import cz.vutbr.fit.pdb.project01.repository.DatabaseRepository;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

/**
 *
 * @author jakub
 */
public class LoginPanel extends JPanel implements ActionListener {
    
    private EstateAgency mainFrame;
    private JLabel statusLabel;
    private JCheckBox initDB;
    private JComboBox userSelect;

    /**
     * Constructor
     * @param mainFrame 
     */
    public LoginPanel(EstateAgency mainFrame) 
    {     
        placeComponents();
        this.mainFrame = mainFrame;
    }
    
    private void placeComponents()
    {
        setLayout(new BorderLayout());
        
        JPanel content = new JPanel();
        JPanel statusPanel = new JPanel();
        
        add(content, BorderLayout.CENTER);
        add(statusPanel, BorderLayout.SOUTH);
        
        
        
        content.setLayout(new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(5, 5, 5, 5);

        JLabel userLabel = new JLabel("Uživatel");
        content.add(userLabel, gbc);
        
        gbc.gridx = 1;
        userSelect = new JComboBox(AuthenticationController.identities);
        content.add(userSelect, gbc);
        
        gbc.gridy = 1;
        gbc.gridx = 0;
        gbc.anchor = GridBagConstraints.CENTER;   
        initDB = new JCheckBox("Inicilaizovat DB");
        initDB.addItemListener(new InitDatabaseListener());
        content.add(initDB, gbc);       
        
        
        gbc.gridy = 1;
        gbc.gridx = 1;
        JButton loginButton = new JButton("Přihlásit se");
        loginButton.addActionListener(this);
        content.add(loginButton, gbc);
        

        /// Status panel
        statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
        statusPanel.setPreferredSize(new Dimension(getWidth(), 16));
        statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
        statusLabel = new JLabel("Připraven");
        statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
        statusPanel.add(statusLabel);
        
    }    

    @Override
    public void actionPerformed(ActionEvent e) 
    {
        //Informace pro uzivatele
        statusLabel.setText("Připojuji se...");
 
        new Thread(new Runnable() {

                @Override
                public void run() 
                {
                    DatabaseRepository dbRepository = DatabaseRepository.GetConfiguration();
                    Database db = Database.getDatabase(dbRepository.DatabaseModel);
                    
                    if(db != null && db.isConnected())
                    {
                        //Ulozeni prihlasovacich udaju do konfiguracniho souboru
                        DatabaseRepository.SaveConfiguration();
                        statusLabel.setText("Připojen");  
                        
                        
                        //initialization DB
                        if(initDB.isSelected())
                        {
                            statusLabel.setText("Inicializace DB");
                            db.initDatabase();
                        }
                        
                        
                        mainFrame.connectionToDatabaseSuccess(db, AuthenticationController.identities[userSelect.getSelectedIndex()]);
                    }
                    else
                    {
                        statusLabel.setText("Chyba při pokusu o přihlášení");
                    }                    
                }
        }).start();       

        
        
    }
}
