package cz.vutbr.fit.pdb.project01.models;

import cz.vutbr.fit.pdb.project01.Debugging;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;

/**
 *
 * @author Jakub Švestka
 */
public class PolygonDrawModel extends BaseDrawModel{

    /**
     * Fake point
     */
    protected Point fakePoint = null;
    
    /**
     * Set clicked point
     */
    protected Point clickedPoint = null;
    
    /**
     * Construcotr
     * @param point 
     */
    public PolygonDrawModel(Point point)
    {
        super(point);   

        shape = new Polygon();

        //Set starting point
        ((Polygon)shape).addPoint((int)point.getX(), (int)point.getY());
        
        
        shape.contains(new Point(0, 0));
        
        Debugging.info("Start ponit of noise zone was created");
    }  
    
    /**
     * Add nex point
     * @param point 
     */
    public void addPoint(Point point)
    {
        removeFakePoint(); //reset
        
        ((Polygon)shape).addPoint((int)point.getX(), (int)point.getY());
        
        Debugging.info("Added next point to noise zone");
    }
    
    /**
     * Add fake point
     * @param point 
     */
    public void addFakePoint(Point point)
    {
        fakePoint = point;
    }   
    
    /**
     * Remove fake point
     */
    public void removeFakePoint()
    {
        fakePoint = null;
    }    
    
    /**
     * Moving with entity
     * @param point 
     */
    @Override
    public void move(Point point) 
    {
        if(clickedPoint == null)
        {
            clickedPoint = point; //first click
            return;
        }
        
        Shape newShape = new Polygon();
        
        double dx = (clickedPoint.getX() - point.getX()); //difference between first and second click
        double dy = (clickedPoint.getY() - point.getY()); //difference between first and second click
        
        boolean first = true;
        for(Point p: getPoints())
        {
            if(first)
            {
                setPosition((int)(p.getX() - dx), (int)(p.getY() - dy));
                first = false;
            }
            
            ((Polygon)newShape).addPoint((int)(p.getX() - dx), (int)(p.getY() - dy));
        }
        
        clickedPoint = point; //use second point as first
        
        shape = newShape; //actualize shape
    }   
    
    
    /**
     * Get shape of entity
     * @return 
     */   
    public Shape getShape()
    {
        if(fakePoint == null)
        {
            return super.getShape();
        }
        else
        {
            Polygon polygon = new Polygon();
            polygon.addPoint(getX(), getY());
            
            PathIterator pi = shape.getPathIterator(null);
            
            float[] returnedValues = new float[6];

            while (pi.isDone() == false) 
            {
                pi.currentSegment(returnedValues);
                polygon.addPoint((int)returnedValues[0], (int)returnedValues[1]);
                pi.next();
            }
            
            //add fake point
            polygon.addPoint((int)fakePoint.getX(), (int)fakePoint.getY());
            
            return polygon;
        }
    }    
    
    /**
     * Count points of line
     * @return 
     */
    public int countPoints()
    {
        return ((Polygon)shape).npoints;
    }
    
        
    /**
     * Get points for inserting to DB
     * @return 
     */
    public String getPointsForSQL()
    {
        String points = new String();
        
        Polygon polygon = (Polygon) shape;


        for(int i=0; i< polygon.npoints; i++)
        {
            points += Integer.toString(polygon.xpoints[i]) + ", " + Integer.toString(polygon.ypoints[i]);
            
            if( (i+1) < polygon.npoints )
                points += ",";
            else
                points += "," + Integer.toString(polygon.xpoints[0]) + ", " + Integer.toString(polygon.ypoints[0]);
        }        
        
        return points;
    }
    
    /**
     * Get points of line
     * @return 
     */
    public Point[] getPoints()
    {
        Point[] points = new Point[countPoints()];

        
        Polygon polygon = (Polygon) shape;

        int j = 0;
        for(int i=0; i< polygon.npoints; i++)
        {
            points[j++]= new Point(polygon.xpoints[i], polygon.ypoints[i]);
        }
        
        return points;
    }    
    
    /**
     * Detection If given point is part of line
     * @param point
     * @return 
     */
    public boolean containsPoint(Point point)
    {
        
        Polygon polygon = (Polygon) shape;
        
        Point startPoint = null, startPointTemp = null, endPoint = null;
        
        
        for(int i=0; i< polygon.npoints; i++)
        {
            if(i == 0)
            {
                startPoint = new Point(polygon.xpoints[i], polygon.ypoints[i]); //Start
                startPointTemp = new Point(polygon.xpoints[i], polygon.ypoints[i]); //Start

                if(i++ >= polygon.npoints)
                    return false;

                endPoint = new Point(polygon.xpoints[i], polygon.ypoints[i]); //End
            }
            else
            {
                startPoint = endPoint; //Start

                endPoint = new Point(polygon.xpoints[i], polygon.ypoints[i]); //End           
            }


            double difference = Line2D.ptSegDist(startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY(),  point.getX(),  point.getY());
            
            //Tolerance 2.0
            if(difference < 2.0 && difference > -2.0)
                return true;     
            
            
            //if last, test the line from first and last point
            if((i + 1) >= polygon.npoints)
            {
                startPoint = startPointTemp; //Start 
                
                difference = Line2D.ptSegDist(startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY(),  point.getX(),  point.getY());

                //Tolerance 2.0
                if(difference < 2.0 && difference > -2.0)
                    return true;                 
            }
        }        
        
        
        
        return false;
    }

    /**
     * Line is hit If contains point
     * @param point
     * @return 
     */
    public boolean isHit(Point point) 
    {
        return containsPoint(point);
    }
    
    /**
     * Removing fake point
     */
    public void clearClickedPoint()
    {
        clickedPoint = null;
    }     
    

}
