/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.models;

/**
 *
 * @author brian
 */
public interface IOffer {
    
    /**
     * Get name
     * @return 
     */
    public String getName();
    
    /**
     * Get type
     * @return 
     */
    public String getType();
    
    /**
     * Get area
     * @return 
     */
    public Integer getArea();
    
    /**
     * Get locality
     * @return 
     */
    public Integer getLocality();
    
    /**
     * Get address
     * @return 
     */
    public String getAddress();
    
    /**
     * Get description
     * @return 
     */
    public String getDescription();
}
