package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.Database;
import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.PhotoModel;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.ord.im.OrdImage;

/**
 * General image storage and processing
 * @author Ondrej Koblizek
 */
public class PhotoRepository {
    private final Connection c;

    public enum ImageProcessing {
        ROTATE_CW, ROTATE_CCW,
        MIRROR_H, MIRROR_V,
        NONE
    }

    /**
     * Constructor
     */
    public PhotoRepository() {
        this.c = Database.getConnection();
    }

    /**
     * Insert new photo into DB
     * @param photo - newly created model
     * @return id of created photo if successful otherwise 0
     */
    public int addPhoto(PhotoModel photo) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int affectedRows;
        int id = 0;

        try {
            c.setAutoCommit(false);

            // insert empty ORDImage
            ps = c.prepareStatement("INSERT INTO PHOTO (photo, mini, title, offerId) VALUES (ordsys.ordimage.init(), ordsys.ordimage.init(), ?, ?)", new String[]{"id"});
            ps.setString(1, photo.getTitle());
            if (photo.getOfferId() != 0) {
                ps.setInt(2, photo.getOfferId());
            } else {
                ps.setNull(2, photo.getOfferId());
            }
            affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating offer failed, no rows affected.");
            }
            rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                id = rs.getInt(1);
                photo.setId(id);
            } else {
                throw new SQLException("Creating offer failed, no ID obtained.");
            }
            ps.close();

            processImages(photo, ImageProcessing.NONE);

            c.commit();
            c.setAutoCommit(true);

        } catch (SQLException ex) {
            Debugging.error("DB addPhoto: " + ex.getMessage() + " , code: " + ex.getErrorCode());
        } finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                Debugging.error("DB addPhoto: " + ex.getMessage());
            }
        }
        return id;
    }

    private void processImages(PhotoModel photo, ImageProcessing ipo) {
        PreparedStatement ps2 = null;
        OraclePreparedStatement ps3;
        OraclePreparedStatement ps4;
        OraclePreparedStatement ps5;
        OracleResultSet ors;

        try {
            // fetch ORDImage
            ps2 = c.prepareStatement("SELECT photo, mini FROM PHOTO WHERE id=? FOR UPDATE");
            ps2.setInt(1, photo.getId());
            Debugging.success("processImages: new id: " + photo.getId());
            ors = (OracleResultSet) ps2.executeQuery();
            if (ors == null || !ors.next()) {
                throw new SQLException("DB processImages: record with id: " + photo.getId() + " not found.");
            }
            OrdImage imgProxy = (OrdImage)ors.getORAData("photo", OrdImage.getORADataFactory());
            OrdImage miniProxy = (OrdImage)ors.getORAData("mini", OrdImage.getORADataFactory());
            ors.close();
            ps2.close();

            // fetch main image
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(photo.getPhoto(), "jpg", baos);

            imgProxy.loadDataFromByteArray(baos.toByteArray());
            imgProxy.setProperties();

            // transform image
            switch (ipo) {
                case ROTATE_CW:
                    imgProxy.process("rotate=\"90.0\" fileformat=jpg");
                    break;
                case ROTATE_CCW:
                    imgProxy.process("rotate=\"-90.0\" fileformat=jpg");
                    break;
                case MIRROR_H:
                    imgProxy.process("flip fileformat=jpg");
                    break;
                case MIRROR_V:
                    imgProxy.process("mirror fileformat=jpg");
                    break;
                default:
                    break;
            }

            //Debugging.success("processImages: checkpoint A");
            // rescale main image into mini
            if (photo.getMiniWidth() <= 0 || photo.getMiniHeight() <= 0) {
                throw new PhotoException("Invalid sizes for minified image: w: " + photo.getMiniWidth() + " h: " + photo.getMiniHeight());
            }
            imgProxy.processCopy("maxscale=" + photo.getMiniWidth() + " " + photo.getMiniHeight() + " fileformat=jpg", miniProxy);

            //Debugging.success("processImages: checkpoint B");
            // save images
            ps3 = (OraclePreparedStatement)c.prepareStatement("UPDATE PHOTO SET photo=?, mini=? WHERE id=?");
            ps3.setORAData(1, imgProxy);
            ps3.setORAData(2, miniProxy);
            ps3.setInt(3, photo.getId());
            ps3.executeUpdate();
            ps3.close();

            //Debugging.success("processImages: checkpoint C");

            // make a StillImage
            ps4 = (OraclePreparedStatement)c.prepareStatement("UPDATE PHOTO p SET p.photoSI=SI_StillImage(p.photo.getContent()) WHERE id=?");
            ps4.setInt(1, photo.getId());
            ps4.executeUpdate();
            ps4.close();

            // make a metrics for StillImage
            ps5 = (OraclePreparedStatement)c.prepareStatement("UPDATE PHOTO p SET p.photoAC=SI_AverageColor(p.photoSI), p.photoCH=SI_ColorHistogram(p.photoSI), p.photoPC=SI_PositionalColor(p.photoSI), p.photoTX=SI_Texture(p.photoSI) WHERE id=?");
            ps5.setInt(1, photo.getId());
            ps5.executeUpdate();
            ps5.close();

            //Debugging.success("processImages: checkpoint D");

        } catch (SQLException ex) {
            Debugging.error("DB processImages: " + ex.getMessage() + " , code: " + ex.getErrorCode());
        } catch (IOException ex) {
            Debugging.error("IO processImages: " + ex);
        } catch (PhotoException ex) {
            Debugging.error("Photo processImages: " + ex);
        } finally {
            try {
                if (ps2 != null) ps2.close();
            } catch (SQLException ex) {
                Debugging.error("DB addPhoto: " + ex.getMessage());
            }
        }
    }

    /**
     * Pick one photo
     * @param id
     * @return found photo or null
     */
    public PhotoModel getPhotoById(int id) {
        OraclePreparedStatement ops = null;
        OracleResultSet ors = null;
        OrdImage oi;
        OrdImage moi;

        try {
            ops = (OraclePreparedStatement)c.prepareStatement("SELECT * FROM PHOTO WHERE id = ?");
            ops.setInt(1, id);
            ors = (OracleResultSet)ops.executeQuery();
            if (ors.next()) {
                oi = (OrdImage)ors.getORAData("photo", OrdImage.getORADataFactory());
                moi = (OrdImage)ors.getORAData("mini", OrdImage.getORADataFactory());
                try {
                    return new PhotoModel(ors.getInt("id"),
                            createImageFromBytes(oi.getDataInByteArray()),
                            moi.getWidth(),
                            moi.getHeight(),
                            createImageFromBytes(moi.getDataInByteArray()),
                            ors.getString("title"),
                            ors.getInt("offerId"));
                } catch (OutOfMemoryError ex) {
                    Logger.getLogger(PhotoRepository.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(PhotoRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ors.close();
            ops.close();
        } catch (SQLException ex) {
            Debugging.error("DB getPhotoById: " + ex.getMessage());
        } finally {
            try {
                if (ors != null) ors.close();
                if (ops != null) ops.close();
            } catch (SQLException ex) {
                Debugging.error("DB getPhotoById: " + ex.getMessage());
            }
        }
        return null;
    }

    /**
     * Pick photos for offerId
     * @param offerId
     * @return found photos or empty list
     */
    public List<PhotoModel> getPhotoByOfferId(int offerId) {
        OraclePreparedStatement ops = null;
        OracleResultSet ors = null;
        OrdImage oi;
        OrdImage moi;
        List<PhotoModel> pms = new ArrayList<>();
        try {
            ops = (OraclePreparedStatement)c.prepareStatement("SELECT * FROM PHOTO WHERE offerId = ?");
            ops.setInt(1, offerId);
            ors = (OracleResultSet)ops.executeQuery();
            while (ors.next()) {
                oi = (OrdImage)ors.getORAData("photo", OrdImage.getORADataFactory());
                moi = (OrdImage)ors.getORAData("mini", OrdImage.getORADataFactory());
                try {
                    PhotoModel pm = new PhotoModel(ors.getInt("id"),
                            createImageFromBytes(oi.getDataInByteArray()),
                            moi.getWidth(),
                            moi.getHeight(),
                            createImageFromBytes(moi.getDataInByteArray()),
                            ors.getString("title"),
                            ors.getInt("offerId"));
                    pms.add(pm);
                } catch (OutOfMemoryError ex) {
                    Logger.getLogger(PhotoRepository.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(PhotoRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ors.close();
            ops.close();
        } catch (SQLException ex) {
            Debugging.error("DB getPhotoById: " + ex.getMessage());
        } finally {
            try {
                if (ors != null) ors.close();
                if (ops != null) ops.close();
            } catch (SQLException ex) {
                Debugging.error("DB getPhotoById: " + ex.getMessage());
            }
        }
        return pms;
    }

    /**
     * Update existing model in DB by id
     * @param photo to update
     * @param ipo transform parameter
     * @return id of updated photo if successful otherwise 0
     */
    public int updatePhoto(PhotoModel photo, ImageProcessing ipo) {
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            c.setAutoCommit(false);

            ps = c.prepareStatement("UPDATE PHOTO SET title=?, offerId=? WHERE id = ?");
            ps.setString(1, photo.getTitle());
            ps.setInt(2, photo.getOfferId());
            ps.setInt(3, photo.getId());
            int rowsUpdated = ps.executeUpdate();

            processImages(photo, ipo);

            c.commit();
            c.setAutoCommit(true);

            return rowsUpdated > 0 ? photo.getId() : 0;
        } catch (SQLException ex) {
            Debugging.error("DB updateOffer: " + ex.getMessage());
        } finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                Debugging.error("DB updateOffers: " + ex.getMessage());
            }
        }

        return 0;
    }

    /**
     * Delete photo from DB by id
     * @param id
     * @return 1 if successful
     */
    public int removePhoto(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = c.prepareStatement("DELETE FROM PHOTO WHERE id = ?");
            ps.setInt(1, id);
            int rowsUpdated = ps.executeUpdate();
            return rowsUpdated;
        } catch (SQLException ex) {
            Debugging.error("DB removePhoto: " + ex.getMessage());
        } finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                Debugging.error("DB removePhoto: " + ex.getMessage());
            }
        }
        return 0;
    }

    private static class PhotoException extends Exception {

        public PhotoException(String string) {
        }
    }

    private BufferedImage createImageFromBytes(byte[] imageData) {
        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        try {
            return ImageIO.read(bais);
        } catch (IOException e) {
            Debugging.error("PhotoRepository createImageFromBytes: " + e.getMessage());
        }
        return null;
    }

    public int searchByPhoto(int queryPhotoId) {
        OraclePreparedStatement ops = null;
        OracleResultSet ors = null;
        int resultId = 0;
        int sameness = 0;

        try {
            //Debugging.info("DB searchByPhoto: queryPhotoId: " + queryPhotoId);
            ops = (OraclePreparedStatement)c.prepareStatement("SELECT dst.id, SI_ScoreByFtrList(new SI_FeatureList(src.photoAC,0.1,src.photoCH,0.1,src.photoPC,0.1,src.photoTX,0.1),dst.photoSI) AS sameness FROM PHOTO src, PHOTO dst WHERE (src.id <> dst.id) AND dst.offerId IS NOT NULL AND src.id = ? ORDER BY sameness ASC");
            ops.setInt(1, queryPhotoId);
            ors = (OracleResultSet)ops.executeQuery();
            if (ors.next()) {
                resultId = ors.getInt(1);
                sameness = 100 - ors.getInt(2);
                Debugging.info("DB searchByPhoto: resultId: " + resultId + ", sameness: " + sameness);
            }
            ors.close();
            ops.close();
        } catch (SQLException ex) {
            Debugging.error("DB searchByPhoto: " + ex.getMessage());
        } finally {
            try {
                if (ors != null) ors.close();
                if (ops != null) ops.close();
            } catch (SQLException ex) {
                Debugging.error("DB searchByPhoto: " + ex.getMessage());
            }
        }
        return resultId;
    }
}
