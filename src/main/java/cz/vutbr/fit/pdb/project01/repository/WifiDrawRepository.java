package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.WifiDrawModel;
import java.awt.Point;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.spatial.geometry.JGeometry;

/**
 *
 * @author brian
 */
public class WifiDrawRepository extends BaseDrawRepository<WifiDrawModel>{

    /**
     * Insert new antity
     * @param entity 
     */
    public void insert(WifiDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            //create entity on map
            pstmt = c.prepareStatement( "INSERT INTO "
                    + "wifi(location, ssid, channel) values("                    
                     + "SDO_GEOMETRY(2003, NULL, "
                        + "NULL,"
                        + "SDO_ELEM_INFO_ARRAY(1, 1003, 4),"
                        + "SDO_ORDINATE_ARRAY(? ,? ,? ,? ,? ,?)" 
                    + "), ?, ?)", new String[]{"id"}); 
            
            pstmt.setInt(1, entity.getX()+entity.getRadius());
            pstmt.setInt(2, entity.getY());
            pstmt.setInt(3, entity.getX()-entity.getRadius());
            pstmt.setInt(4, entity.getY());
            pstmt.setInt(5, entity.getX());
            pstmt.setInt(6, entity.getY()-entity.getRadius());
            pstmt.setString(7, entity.getSSID());  //ssid
            pstmt.setInt(8, entity.getChannel());  //channel            
            
            pstmt.execute();
            
            if(pstmt.getUpdateCount() > 0)
            {            
                try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        int ID = generatedKeys.getInt(1);
                        
                        entity.setId(ID);

                        Debugging.info("Entity ID is: " + Integer.toString(ID));                   
                    }
                    else {
                        Debugging.error("Creating entity failed, not MAP_ID obtained.");
                    }
                } 
            }
            

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with creating record in CITY_MAP");
        }
              
    }

    /**
     * Update position of entity
     * @param entity 
     */
    public void updatePosition(WifiDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            JGeometry j_geom= JGeometry.createCircle(entity.getX(), entity.getY(), entity.getRadius(), 0);
            pstmt = c.prepareStatement("UPDATE wifi set location=? where id = ?");  

            pstmt.setInt(2, entity.getId());

            Object obj = JGeometry.storeJS(j_geom, c);

            //Update position
            if(obj != null){
                pstmt.setObject(1, obj);

                if(pstmt.executeUpdate() > 0)
                {
                    Debugging.info("Entity was updated");
                } 
            }

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating position in CITY_MAP");
        }     
    }    
    
    /**
     * Update entity
     * @param entity 
     */
    public void update(WifiDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            pstmt = c.prepareStatement("UPDATE wifi SET ssid = ?, channel = ? WHERE id = ?");
            
            pstmt.setString(1, entity.getSSID());
            pstmt.setInt(2, entity.getChannel());
            pstmt.setInt(3, entity.getId());
            
            if (pstmt.executeUpdate() > 0)
            {  
                Debugging.info("Entity was updated");
            }

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating entity");
        }     
    } 
    
    /**
     * Get all records
     * @return 
     */
    public List<WifiDrawModel> getAll()
    {
        List<WifiDrawModel> entities = new ArrayList<>();
        
            try {
                Statement stmt = c.createStatement();
                ResultSet rset = stmt.executeQuery("SELECT wifi.id, wifi.ssid, channel, wifi.location, SDO_GEOM.SDO_CENTROID(wifi.location, m.diminfo) FROM wifi, USER_SDO_GEOM_METADATA m WHERE m.table_name = 'WIFI'");
                

                while (rset.next()) 
                {
                     //get location
                    JGeometry jgeom = JGeometry.loadJS((Struct)rset.getObject(5));

                    int centerX = (int)(jgeom.getPoint()[0]);
                    int centerY = (int)(jgeom.getPoint()[1]);

                    //Radius
                    jgeom = JGeometry.loadJS( (Struct)rset.getObject(4) );

                    //Create object of entity
                    WifiDrawModel entity = new WifiDrawModel(new Point(centerX,centerY), 0, rset.getString(2), rset.getInt(3)); //radius 0
                    entity.calculateAndSetSize(new Point((int)jgeom.getOrdinatesArray()[4],(int)jgeom.getOrdinatesArray()[5])); //radius correction
                    entity.setId(rset.getInt(1));   

                    //add entity to list
                    entities.add(entity);
                }
     
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(BusStopDrawRepository.class.getName()).log(Level.SEVERE, null, ex);
            }        
            
            
        return entities;
    }
    
    

    /**
     * Remove entity
     * @param entity 
     */
    public void remove(WifiDrawModel entity) 
    {
        Statement stmt;

        try {
            stmt = c.createStatement();               
            

            stmt.executeUpdate("DELETE FROM wifi WHERE id = " + entity.getId());

            Debugging.info("Entity was removed");

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with removing entity");
        }            
    }      
}
