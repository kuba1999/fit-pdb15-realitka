
package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.Database;
import java.sql.Connection;
import java.util.List;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 * @param <T>         
 */
public abstract class BaseDrawRepository<T> {

    /**
     * Connection
     */
    protected Connection c;

    /**
     * Constructor
     */
    public BaseDrawRepository() 
    {
        this.c = Database.getConnection();
    }       
    
    /**
     * Remove entity
     * @param entity 
     */
    abstract public void remove(T entity);
    
    /**
     * Get all entities
     * @return 
     */
    abstract public List<T> getAll();
}
