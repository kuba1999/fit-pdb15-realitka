package cz.vutbr.fit.pdb.project01.models;

import java.awt.Color;
import java.awt.Point;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class SchoolDrawModel extends PointDrawModel{
    
    /**
     * Name of school
     */
    private String name;

    /**
     * Constructor
     * @param point
     * @param name 
     */
    public SchoolDrawModel(Point point, String name) 
    {
        super(point);
        
        this.name = name;
        
        setFillColor(Color.RED);
        setStrokeColor(Color.BLACK);
    }
    
    /**
     * Get name
     * @return 
     */
    public String getName()
    {
        return name;
    }   
    
    /**
     * Set name
     * @param name 
     */
    public void setName(String name)
    {
        this.name = name;
    }    
}
