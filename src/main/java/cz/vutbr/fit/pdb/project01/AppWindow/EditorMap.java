/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;


import cz.vutbr.fit.pdb.project01.Debugging;
import java.awt.Font;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JButton;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;

/**
 * @author      Karolína Hajná      <xhajna00@stud.fit.vutbr.cz>
 * @version     1.0                 
 * @since       2015-10-26          
 */
public class EditorMap extends JPanel{

    /**
     * Type of entities
     */
    public static enum typeOfentities{
        SCHOOL, BUS_STOP, BUILDINGDS, LOT, WIFI, ROAD, NOISE_ZONE
    };
    
    private typeOfentities selectedTypeOfEntity;
    
    /**
    * area for drawing map and entities in DB
    */
    private EditDrawArea drawArea;
    
    HashMap<typeOfentities, JToggleButton> createEntityButtons = new HashMap<typeOfentities, JToggleButton>();
    
    /**
    * Check boxes showing/hiding certain type of entities on map
    */
    public JCheckBox offersChk, wifiChk, roadsChk, noiseChk, busChk, schoolChk;
    
    /**
    * button for clearing made changes
    */
    public JButton clearBtn;
    
    /**
    * constants for showing proper form when entering new entity
    */
    static final int FLAT = 0;
    static final int AREA = 1;
    
    /**
     * main application window
     */
    private AppWindow mainFrame;
    
    /**
     * Initializes window appearance
     * 
     * @param mainFrame main application window
     */
    public EditorMap(AppWindow mainFrame)
    {
        this.mainFrame = mainFrame;
        
        clearBtn = new JButton("Smazat");      
        offersChk = new JCheckBox("Nabídky");
        wifiChk = new JCheckBox("Pokrytí WiFi");
        roadsChk = new JCheckBox("Silnice");
        noiseChk = new JCheckBox("Hlukové Zóny");
        busChk = new JCheckBox("Autobusové zastávky");
        schoolChk = new JCheckBox("Školy");
        
        
        setLayout(new GridBagLayout());
        GridBagConstraints gridConstraints = new GridBagConstraints();
  
        drawArea = new EditDrawArea(this);
        
        
        JScrollPane scroll = new JScrollPane(drawArea);
        scroll.setAutoscrolls(true);
        gridConstraints.insets = new Insets(5, 5, 5, 0);
        gridConstraints.fill = GridBagConstraints.BOTH;
        gridConstraints.gridheight = 20;
        gridConstraints.gridwidth = 20;
        gridConstraints.gridx = 0;
        gridConstraints.gridy = 0;
        gridConstraints.weightx = 25;
        gridConstraints.weighty = 90;
        add(scroll, gridConstraints);   

        gridConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridConstraints.insets = new Insets(20, 5, 5, 5);
        gridConstraints.gridheight = 1;
        gridConstraints.gridwidth = 1;
        gridConstraints.weightx = 0;
        gridConstraints.weighty = 0;

        Font font;
        JLabel label = new JLabel("Přidat objekt");
        label.setFont(font = new Font(null, Font.BOLD, 15));
        gridConstraints.gridx = 20;
        gridConstraints.gridy = 0;
        add(label, gridConstraints);

        JToggleButton button;
        button = new JToggleButton("Dům / Byt");
        createEntityButtons.put(typeOfentities.BUILDINGDS, button);
        gridConstraints.insets = new Insets(5, 5, 5, 5);
        gridConstraints.gridy = 1;
        add(button, gridConstraints);

        button = new JToggleButton("Pozemek");
        createEntityButtons.put(typeOfentities.LOT, button);
        gridConstraints.gridy = 2; 
        add(button, gridConstraints); 
        
        button = new JToggleButton("Pokrytí Wifi");
        createEntityButtons.put(typeOfentities.WIFI, button);
        gridConstraints.gridy = 3; 
        add(button, gridConstraints);
        
        button = new JToggleButton("Silnice");
        createEntityButtons.put(typeOfentities.ROAD, button);
        gridConstraints.gridy = 4; 
        add(button, gridConstraints);
        
        button = new JToggleButton("Hluková zóna");
        createEntityButtons.put(typeOfentities.NOISE_ZONE, button);
        gridConstraints.gridy = 5; 
        add(button, gridConstraints);
        
        button = new JToggleButton("Škola");
        createEntityButtons.put(typeOfentities.SCHOOL, button);
        gridConstraints.gridy = 6; 
        add(button, gridConstraints);
        
        button = new JToggleButton("MHD zastávka");
        createEntityButtons.put(typeOfentities.BUS_STOP, button);
        gridConstraints.gridy = 7; 
        add(button, gridConstraints);        
        
        JSeparator sep = new JSeparator(JSeparator.CENTER);
        gridConstraints.insets = new Insets(15, 5, 15, 5);
        gridConstraints.gridy = 8;
        add(sep, gridConstraints);     
        
        label = new JLabel("Zobrazit objekty");
        label.setFont(new Font(font.getAttributes()));
        gridConstraints.insets = new Insets(5, 5, 5, 5);
        gridConstraints.gridy = 10;
        add(label, gridConstraints);
        
        offersChk.setSelected(true);
        gridConstraints.gridy = 11;
        add(offersChk, gridConstraints);

        wifiChk.setSelected(true);
        gridConstraints.gridy = 12;
        add(wifiChk, gridConstraints);
        
        roadsChk.setSelected(true);
        gridConstraints.gridy = 13;
        add(roadsChk, gridConstraints);
        
        noiseChk.setSelected(true);
        gridConstraints.gridy = 14;
        add(noiseChk, gridConstraints);
        
        schoolChk.setSelected(true);
        gridConstraints.gridy = 15;
        add(schoolChk, gridConstraints);
        
        busChk.setSelected(true);
        gridConstraints.gridy = 16;
        add(busChk, gridConstraints);
        
        sep = new JSeparator(JSeparator.CENTER);
        gridConstraints.insets = new Insets(15, 5, 15, 5);
        gridConstraints.gridy = 17;
        add(sep, gridConstraints);
        
        
        gridConstraints.insets = new Insets(5, 5, 5, 5);
        gridConstraints.gridy = 18; 
        add(clearBtn, gridConstraints);
           
        bindEventsToButtons();
    }
    
    private void bindEventsToButtons() {
        for(Map.Entry<typeOfentities, JToggleButton> entry: createEntityButtons.entrySet())
        {
            JToggleButton button = entry.getValue();

            button.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent ev) {
                   if(ev.getStateChange()==ItemEvent.SELECTED)
                   {
                        //deselect other buttons
                        DeselectEntityButtons((JToggleButton) ev.getSource());


                        if (ev.getSource() == createEntityButtons.get(typeOfentities.SCHOOL)) 
                        {
                            selectedTypeOfEntity = typeOfentities.SCHOOL;
                        }
                        else if (ev.getSource() == createEntityButtons.get(typeOfentities.BUS_STOP)) 
                        {
                            selectedTypeOfEntity = typeOfentities.BUS_STOP;
                        }
                        else if (ev.getSource() == createEntityButtons.get(typeOfentities.WIFI)) 
                        {
                            selectedTypeOfEntity = typeOfentities.WIFI;
                        }
                        else if (ev.getSource() == createEntityButtons.get(typeOfentities.ROAD)) 
                        {
                            selectedTypeOfEntity = typeOfentities.ROAD;
                        }
                        else if (ev.getSource() == createEntityButtons.get(typeOfentities.NOISE_ZONE)) 
                        {
                            selectedTypeOfEntity = typeOfentities.NOISE_ZONE;
                        }
                        else if (ev.getSource() == createEntityButtons.get(typeOfentities.BUILDINGDS)) 
                        {
                            selectedTypeOfEntity = typeOfentities.BUILDINGDS;
                        }
                        else if (ev.getSource() == createEntityButtons.get(typeOfentities.LOT)) 
                        {
                            selectedTypeOfEntity = typeOfentities.LOT;
                        }

                        Debugging.info("Vytvorime novou entitu: " + selectedTypeOfEntity.name());

                   }

                   if(ev.getStateChange()==ItemEvent.DESELECTED)
                   {
                       Debugging.info("Nebudeme vytvaret nic");
                       selectedTypeOfEntity = null;
                   }
                }
             });       
        }
    }    
    
    public void DeselectEntityButtons(JToggleButton activeButton)
    {
        for(Map.Entry<typeOfentities, JToggleButton> entry: createEntityButtons.entrySet())
        {
            JToggleButton button = entry.getValue();
            if(button != activeButton)
                button.setSelected(false);
        }        
    }
    
    public typeOfentities getSelectedTypeOfEntity()
    {
        return selectedTypeOfEntity;
    }
    
    /**
     * Get main fram of application
     * @return 
     */
    public AppWindow getMainFrame()
    {
        return mainFrame;
    }
}
