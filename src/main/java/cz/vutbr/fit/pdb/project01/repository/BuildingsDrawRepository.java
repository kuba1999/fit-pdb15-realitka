/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.BuildingDrawModel;
import cz.vutbr.fit.pdb.project01.models.BuildingDrawModel;
import cz.vutbr.fit.pdb.project01.models.NoisyZoneDrawModel;
import java.awt.Point;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.spatial.geometry.JGeometry;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class BuildingsDrawRepository extends BaseDrawRepository<BuildingDrawModel>{
    
    /**
     * Insert new entity
     * @param entity 
     */
    public void insert(BuildingDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            //create on map
            pstmt = c.prepareStatement( "INSERT INTO "
                + "offers(location, name, type, area, locality, address, description, \"user\", building) values("                    
                + "SDO_GEOMETRY(2001, NULL, "
                    + "SDO_POINT_TYPE(?,?,NULL),"
                    + "NULL, NULL" 
                + "), ?, ?, ?, ?, ?, ?, ?, 1)", new String[]{"id"});  
        

            pstmt.setString(1, Integer.toString(entity.getX()));
            pstmt.setString(2, Integer.toString(entity.getY()));
            
            pstmt.setString(3, entity.getName());
            pstmt.setString(4, entity.getType());
            pstmt.setInt(5, entity.getArea());
            pstmt.setInt(6, entity.getLocality());
            pstmt.setString(7, entity.getAddress());
            pstmt.setString(8, entity.getDescription());
            pstmt.setString(9, entity.getUser());           
            
            pstmt.execute();

            
            if(pstmt.getUpdateCount() > 0)
            {            
                try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        int ID = generatedKeys.getInt(1);

                        Debugging.info("Entity was created");                      
                        entity.setId(ID);

                        Debugging.info("Entity ID is: " + Integer.toString(ID));
                                            
                    }
                    else {
                        Debugging.error("Creating entity failed, not MAP_ID obtained.");
                    }
                } 
            }
            

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with creating record in CITY_MAP");
        }
              
    }

    /**
     * Update position of entity
     * @param entity 
     */
    public void updatePosition(BuildingDrawModel entity)
    {
        PreparedStatement pstmt;      

        
        try {
            JGeometry j_geom=new JGeometry(entity.getX(), entity.getY(), 0);
            pstmt = c.prepareStatement("UPDATE offers set location=? where id = " + entity.getId());  

            Object obj = JGeometry.storeJS(j_geom, c);

            //Update position
            if(obj != null){
                pstmt.setObject(1, obj);

                if(pstmt.executeUpdate() > 0)
                {
                    Debugging.info("Entity was updated");
                } 
            }

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating position in CITY_MAP");
        }     
    }    
    
    /**
     * Update entity
     * @param entity 
     */
    public void update(BuildingDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            pstmt = c.prepareStatement("UPDATE offers SET name = ?, type = ?, area = ?, locality = ?, address = ?, description = ? WHERE id = ?");
            
            pstmt.setString(1, entity.getName());
            pstmt.setString(2, entity.getType());
            pstmt.setInt(3, entity.getArea());
            pstmt.setInt(4, entity.getLocality());
            pstmt.setString(5, entity.getAddress());
            pstmt.setString(6, entity.getDescription());
            pstmt.setInt(7, entity.getId());
            
            if (pstmt.executeUpdate() > 0)
            {  
                Debugging.info("Entity was updated");
            }

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating entity");
        }     
    } 
    
    /**
     * Get all records
     * @return 
     */
    public List<BuildingDrawModel> getAll()
    {
        List<BuildingDrawModel> entities = new ArrayList<>();
        
            try {
                Statement stmt = c.createStatement();
                ResultSet rset = stmt.executeQuery("SELECT id, name, type, area, locality, address, description, \"user\", location FROM offers WHERE building = 1 and solddate IS NULL");
                           


                while (rset.next()) 
                {
                    //get location
                    JGeometry jgeom = JGeometry.loadJS((Struct)rset.getObject(9) );

                    //Create object of entity
                    BuildingDrawModel entity = new BuildingDrawModel(new Point((int)jgeom.getPoint()[0],(int)jgeom.getPoint()[1]));
                    entity.setId(rset.getInt(1));  
                    entity.setName(rset.getString(2));
                    entity.setType(rset.getString(3));
                    entity.setArea(rset.getInt(4));
                    entity.setLocality(rset.getInt(5));
                    entity.setAddress(rset.getString(6));
                    entity.setDescription(rset.getString(7));
                    entity.setUser(rset.getString(8));

                    //add entity to list
                    entities.add(entity);
                    
                }
     
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(BuildingsDrawRepository.class.getName()).log(Level.SEVERE, null, ex);
            }        
            
            
        return entities;
    }
    

    /**
     * Remove entity
     * @param entity 
     */
    @Override
    public void remove(BuildingDrawModel entity) 
    {
        Statement stmt;

        try {
            stmt = c.createStatement();               
            

            //stmt.executeUpdate("DELETE FROM offers WHERE id = " + entity.getId());
            stmt.executeUpdate("UPDATE offers SET solddate = CURRENT_TIMESTAMP WHERE id = " + entity.getId());

            Debugging.info("Entity was removed");

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with removing entity");
        }              
    }    
}
