/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.Database;
import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import cz.vutbr.fit.pdb.project01.models.BuildingDrawModel;
import cz.vutbr.fit.pdb.project01.models.LotDrawModel;
import cz.vutbr.fit.pdb.project01.models.PhotoModel;
import cz.vutbr.fit.pdb.project01.models.SearchSelectionDrawModel;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import oracle.spatial.geometry.JGeometry;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class SearchOffersRepository {
    
    /**
     * Connection to DB
     */
    protected Connection c;
    
    List<String> whereCondition = new ArrayList<String>();
    List<String> fromTables = new ArrayList<String>();
    private Object ArrayUtils;
    private boolean join = false;
    private int searchByImage = 0;

    /**
     * Constructor
     */
    public SearchOffersRepository() 
    {
        this.c = Database.getConnection();
    }
     
    /**
     * Searching
     * @return 
     */
    public List<BaseDrawModel> search()
    {
        List<BaseDrawModel> entities = new ArrayList<>();
        String map_location_id;
        
            try {
                Statement stmt = c.createStatement();
                
/*                Debugging.info("SELECT id, name, type, area, locality, address, description, \"user\", location "
                        + "FROM offers WHERE id IN ("
                        + "SELECT DISTINCT o.id FROM "
                        + "offers o "
                        + (join ? " FULL OUTER JOIN offers o2 ON o.id != o2.id ":"")
                        + (!fromTables.isEmpty() ?  " , ":"")
                        + join(", ", fromTables)
                        + (!whereCondition.isEmpty() ?  " WHERE " + join(" and ", whereCondition):"")
                        + ")");*/
                //Debugging.warning("B searchByImage: " + searchByImage);
                ResultSet rset = stmt.executeQuery("SELECT id, name, type, area, locality, address, description, \"user\", location "
                        + "FROM offers WHERE id IN ("
                        + "SELECT DISTINCT o.id FROM "
                        + "offers o "
                        + (join ? " FULL OUTER JOIN offers o2 ON o.id != o2.id ":"")
                        + (!fromTables.isEmpty() ?  " , ":"")
                        + join(", ", fromTables)
                        + (!whereCondition.isEmpty() ?  " WHERE " + join(" and ", whereCondition):"")
                        + ")"
                );

                while (rset.next()) 
                {
                    if(rset.getString(3) != null) //building
                    {
                        //get location
                        JGeometry jgeom = JGeometry.loadJS((Struct)rset.getObject(9) );

                        //Create object of entity
                        BuildingDrawModel entity = new BuildingDrawModel(new Point((int)jgeom.getPoint()[0],(int)jgeom.getPoint()[1]));
                        entity.setId(rset.getInt(1));  
                        entity.setName(rset.getString(2));
                        entity.setType(rset.getString(3));
                        entity.setArea(rset.getInt(4));
                        entity.setLocality(rset.getInt(5));
                        entity.setAddress(rset.getString(6));
                        entity.setDescription(rset.getString(7));
                        entity.setUser(rset.getString(8));

                        //add entity to list
                        entities.add(entity);                            
                    }
                    else
                    {
                        //get location
                        JGeometry jgeom = JGeometry.loadJS((Struct)rset.getObject(9) );

                        LotDrawModel entity;

                        if(jgeom.getOrdinatesArray().length < 4) //but road has to have minimal two points => 2 * X,Y => 4
                            continue;

                        //Create object of entity
                        entity = new LotDrawModel(new Point((int)jgeom.getOrdinatesArray()[0], (int)jgeom.getOrdinatesArray()[1]));
                        entity.setId(rset.getInt(1));  
                        entity.setName(rset.getString(2));
                        //entity.setType(rset.getString(3));
                        entity.setArea((new LotDrawRepository()).getArea(entity));
                        entity.setLocality(rset.getInt(5));
                        entity.setAddress(rset.getString(6));
                        entity.setDescription(rset.getString(7));
                        entity.setUser(rset.getString(8));

                        for(int i = 2; i < jgeom.getOrdinatesArray().length; i += 2)
                        {
                            entity.addPoint(new Point((int)jgeom.getOrdinatesArray()[i], (int)jgeom.getOrdinatesArray()[i+1]));
                        }       

                        //add entity to list
                        entities.add(entity);
                    }
                }
     
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(BuildingsDrawRepository.class.getName()).log(Level.SEVERE, null, ex);
            }        
            
            
        return entities;
    }    
    
    /**
     * Show only buildings
     */
    public void onlyBuildings()
    {
        whereCondition.add("o.building = 1");
    }  
    
    /**
     * Show only lost
     */
    public void onlyLots()
    {
        whereCondition.add("o.building = 0");
    }
    
    /**
     * Set localities
     * @param selectedIndices 
     */
    public void inLocalities(int[] selectedIndices) 
    {
        String localities[] = new String[selectedIndices.length];
        
        int i = 0;
        for(Integer locality: selectedIndices)
        {
            localities[i++] = (++locality).toString();
        }
        
        
        whereCondition.add("o.locality IN ("+ join(", ", localities) +")");
    }
    
    /**
     * Covered by WifI
     */
    public void inWifiAP()
    {
        whereCondition.add("(SDO_RELATE(o.location, w.location, 'mask=INSIDE+COVEREDBY') = 'TRUE')");
        fromTables.add("wifi w");
    }
    
    /**
     * Not covered by WifI
     */
    public void notInWifiAP()
    {
        whereCondition.add("(SDO_RELATE(o.location, w.location, 'mask=INSIDE+COVEREDBY') != 'TRUE')");
        fromTables.add("wifi w");
    }
    
    /**
     * In noise
     */
    public void inNoise()
    {
        whereCondition.add("(SDO_RELATE(o.location, n.location, 'mask=INSIDE+COVEREDBY') = 'TRUE')");
        fromTables.add("noisyZones n");
    }
    
    /**
     * Not in noise
     */
    public void notInNoise()
    {
        whereCondition.add("(SDO_RELATE(o.location, n.location, 'mask=INSIDE+COVEREDBY') != 'TRUE')");
        fromTables.add("noisyZones n");
    }    
    
    /**
     * Set school distance
     * @param from
     * @param to 
     */
    public void schoolDistance(String from, String to)
    {
       int f = 0, t = 0;
        
       try {
         f = Integer.parseInt(from);
       } catch (NumberFormatException e) {
         
       }
       
       try {
         t = Integer.parseInt(to);
       } catch (NumberFormatException e) {
         
       }      
        
        whereCondition.add("SDO_GEOM.SDO_DISTANCE(o.location, s.location, 1) BETWEEN "+f/2+" AND "+t/2);
        fromTables.add("schools s");                
    }    
    
    /**
     * Set road distance
     * @param from
     * @param to 
     */
    public void roadDistance(String from, String to)
    {
       int f = 0, t = 0;
        
       try {
         f = Integer.parseInt(from);
       } catch (NumberFormatException e) {
         
       }
       
       try {
         t = Integer.parseInt(to);
       } catch (NumberFormatException e) {
         
       }      
        
        whereCondition.add("SDO_GEOM.SDO_DISTANCE(o.location, r.location, 1) BETWEEN "+f/2+" AND "+t/2);
        fromTables.add("roads r");                
    }
    
    /**
     * Set bus distance 
     * @param from
     * @param to 
     */
    public void busDistance(String from, String to)
    {
       int f = 0, t = 0;
        
       try {
         f = Integer.parseInt(from);
       } catch (NumberFormatException e) {
         
       }
       
       try {
         t = Integer.parseInt(to);
       } catch (NumberFormatException e) {
         
       }      
        
        whereCondition.add("SDO_GEOM.SDO_DISTANCE(o.location, b.location, 1) BETWEEN "+f/2+" AND "+t/2);
        fromTables.add("busStops r");                
    }
    
    /**
     * Set bus distance 
     * @param distance
     */
    public void existsOfferDistance(String distance)
    {
       int d = 0;
        
       try {
         d = Integer.parseInt(distance);
       } catch (NumberFormatException e) {
         
       }   
        
        whereCondition.add("SDO_GEOM.SDO_DISTANCE(o.location, o2.location, 1) BETWEEN 0 AND " + d/2);
        join = true;
    }
    
    /**
     * Filter by photo
     * @param photoPath absolute path to image
     */
    public void byPhoto(String photoPath)
    {
        if (photoPath.length() == 0) {
            return;
        }

        File file = new File(photoPath);
        if (! file.exists()) {
            Debugging.error(SearchOffersRepository.class.getName() + ": file not exists.");
            return;
        }

        try {
            searchByImage = PhotoModel.findByPhoto(ImageIO.read(file));
            whereCondition.add("(p.offerId = o.id AND p.id = " + searchByImage + ")");
            fromTables.add("photo p");
        } catch (IOException ex) {
            Logger.getLogger(SearchOffersRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Set selected area
     * @param entity 
     */
    public void inSelectedArea(SearchSelectionDrawModel entity)
    {
       
        entity.getPointsForSQL();

        whereCondition.add("(SDO_RELATE(o.location, SDO_GEOMETRY(2003, NULL, NULL, "
                    + "SDO_ELEM_INFO_ARRAY(1,1003,1),"
                    + "SDO_ORDINATE_ARRAY("+ entity.getPointsForSQL() +")), 'mask=ANYINTERACT') = 'TRUE')");
    }
    
    /**
     * Set from date
     * @param date 
     */
    public void setFromDate(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("M-y"); //Hours:Minutes:Seconds
        String strDate = dateFormat.format(date);       
        
        
        whereCondition.add("(to_char(o.soldDate, 'MM-YYYY') = '"+ strDate +"' OR o.soldDate IS NULL)"); //date
        
        if(join)
        {
            whereCondition.add("(to_char(o2.soldDate, 'MM-YYYY') = '"+ strDate +"' OR o2.soldDate IS NULL)"); //date
        }
    }
    
    /**
     * Join string list
     * @param delimeter
     * @param array
     * @return 
     */
    private String join(String delimeter, List<String> array)
    {
        String ret = "";
        
        for(String s: array)
        {
            if("".equals(ret))
                ret += s;
            else
                ret += delimeter + s;
        }
        
        return ret;
    }
    
    /**
     * Join string array
     * @param delimeter
     * @param array
     * @return 
     */
    private String join(String delimeter, String array[])
    {
        String ret = "";
        
        for(String s: array)
        {
            if("".equals(ret))
                ret += s;
            else
                ret += delimeter + s;
        }
        
        return ret;
    }
}
