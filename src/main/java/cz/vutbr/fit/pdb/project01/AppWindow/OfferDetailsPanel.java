/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import cz.vutbr.fit.pdb.project01.models.IOffer;
import cz.vutbr.fit.pdb.project01.models.PhotoModel;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;

/**
 * @author      Karolína Hajná      <xhajna00@stud.fit.vutbr.cz>
 * @version     1.0                 
 * @since       2015-10-30          
 */
public class OfferDetailsPanel extends JPanel{
    
    /**
    * constraints for placing GUI components
    */
    GridBagConstraints gbc;
    /**
    * font used for offer name
    */
    Font nameFont = new Font(null, Font.BOLD, 22);
    /**
    * Selected image
    */
    JLabel largeImage;
    /**
    * currently shown miniature images
    */
    BufferedImage image1;
    List<PhotoModel> pms;
    int selectedMiniature;
    /**
    * panel containing tabs
    */
    Image scaledImage;
    int imHeight;
    int imWidth;
    double newWidth;
    /**
    * clickable currently shown miniatures
    */
    List<JButton> miniatures = new ArrayList<JButton>();
    /**
    * dialog holding this tab
    */
    AppWindow parent;
    /**
    * this panel
    */
    OfferDetailsPanel panel = this;
    /**
    * labels for offer details
    */
    JLabel nameLabel, typeLabel, locLabel, addrLabel, areaLabel, priceLabel;
    /**
    * area for offer description
    */
    JTextArea descArea;
    
    /**
     * Initializes tab appearance
     * 
     * @param entity
     * @param p dialog holding this tab
     * @throws java.io.IOException
     */
    public OfferDetailsPanel(BaseDrawModel entity, AppWindow p) throws IOException {
        selectedMiniature = 0;
        pms = PhotoModel.findByOfferId(entity.getId());
        parent = p;
        setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.insets = new Insets(5, 5, 5, 5);
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 21;
        nameLabel = new JLabel("");
        nameLabel.setFont(nameFont);
        add(nameLabel, gbc);
        
        JSeparator sep = new JSeparator(JSeparator.CENTER);
        gbc.gridy = 1;
        add(sep, gbc);
        
        gbc.gridy = 2;
        gbc.gridheight = 5;
        gbc.gridwidth = 5;
        if (!pms.isEmpty()) {
            BufferedImage mainImage = pms.get(selectedMiniature).getPhoto();
            imHeight = mainImage.getHeight();
            imWidth = mainImage.getWidth();
            newWidth = imWidth*(400/(double)imHeight);
            scaledImage = mainImage.getScaledInstance((int) newWidth, 400, Image.SCALE_DEFAULT);
            largeImage = new JLabel(new ImageIcon(scaledImage));
            add(largeImage, gbc);
        
            // Button for listing in the miniautres of pictures to left
            gbc.gridy = 7;
            gbc.gridwidth = 1;
            gbc.gridheight = 1;
            gbc.fill = GridBagConstraints.NONE;
            BufferedImage arrow = ImageIO.read(new File("img/left_arrow.png"));
            imHeight = arrow.getHeight();
            imWidth = arrow.getWidth();
            newWidth = imWidth*(80/(double)imHeight);
            scaledImage = arrow.getScaledInstance((int) newWidth, 80, Image.SCALE_DEFAULT);
            JButton lArrowButton = new JButton(new ImageIcon(scaledImage));
            lArrowButton.addActionListener(new ActionListener() {
                    @Override
                    // Setting miniature as large image
                    public void actionPerformed(ActionEvent ae) {
                        if (selectedMiniature > 0) {
                            image1 = pms.get(--selectedMiniature).getPhoto();
                            refreshMainImage();
                        }
                    }
            });
            lArrowButton.setContentAreaFilled(false);
            add(lArrowButton, gbc);
        
            // Clickable miniatures which can be enlarged
            for (int i = 0; i < pms.size(); i++) {
                PhotoModel pm = pms.get(i);
                gbc.gridx = i+1;
                image1 = pm.getMini();
                imHeight = image1.getHeight();
                imWidth = image1.getWidth();
                newWidth = imWidth*(80/(double)imHeight);
                scaledImage = image1.getScaledInstance((int) newWidth, 80, Image.SCALE_DEFAULT);
                JButton miniButton = new JButton(new ImageIcon(scaledImage));
                miniButton.addActionListener(new ActionListener() {
                    @Override
                    // Setting miniature as large image
                    public void actionPerformed(ActionEvent ae) {
                        image1 = PhotoModel.findById(getClickedPhotoId(ae)).getPhoto();
                        refreshMainImage();
                    }
                });
                miniatures.add(miniButton);
                miniButton.setContentAreaFilled(false);
                add(miniButton, gbc);
            }

            // Button for listing in the miniautres of pictures to right
            gbc.gridx = 4;
            arrow = ImageIO.read(new File("img/right_arrow.png"));
            imHeight = arrow.getHeight();
            imWidth = arrow.getWidth();
            newWidth = imWidth*(80/(double)imHeight);
            scaledImage = arrow.getScaledInstance((int) newWidth, 80, Image.SCALE_DEFAULT);
            JButton rArrowButton = new JButton(new ImageIcon(scaledImage));
            rArrowButton.addActionListener(new ActionListener() {
                    @Override
                    // Setting miniature as large image
                    public void actionPerformed(ActionEvent ae) {
                        if (selectedMiniature < miniatures.size()-1) {
                            image1 = pms.get(++selectedMiniature).getPhoto();
                            refreshMainImage();
                        }
                    }
            });
            rArrowButton.setContentAreaFilled(false);
            add(rArrowButton, gbc);
        } else {
            gbc.gridy = 7;
            gbc.gridwidth = 1;
            gbc.gridheight = 1;
            gbc.fill = GridBagConstraints.NONE;
        }

        gbc.gridx = 5;
        gbc.gridy = 2;
        
        add(new JLabel("Typ:"), gbc);
        
        gbc.gridy = 3;
        add(new JLabel("Lokalita:"), gbc);
        
        gbc.gridy = 4;
        add(new JLabel("Adresa:"), gbc);
        
        gbc.gridy = 5;
        add(new JLabel("Plocha:"), gbc);
        
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 6;
        gbc.gridy = 2;
        gbc.weightx = 1.0;
         
        if(((IOffer)entity).getType() == "")
            typeLabel = new JLabel("pozemek");
        else if(((IOffer)entity).getType() == "house")
            typeLabel = new JLabel("dům");
        else
            typeLabel = new JLabel("byt");
        
        add(typeLabel, gbc);
        
        gbc.gridy = 3;
        locLabel = new JLabel(ResultsPanel.localityConverter(((IOffer)entity).getLocality()));
        add(locLabel, gbc);
        
        gbc.gridy = 4;
        addrLabel = new JLabel(((IOffer)entity).getAddress());
        add(addrLabel, gbc);
        
        gbc.gridy = 5;
        areaLabel = new JLabel(Integer.toString(((IOffer)entity).getArea()) + " m2");
        add(areaLabel, gbc);

        
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.gridwidth = 7;
        descArea = new JTextArea(10, 20);
        descArea.setBackground(nameLabel.getBackground());
        JScrollPane scrollPane = new JScrollPane(descArea); 
        descArea.setEditable(false);
        add(descArea, gbc);
        
        descArea.setText(((IOffer)entity).getDescription());
        
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.gridx = 0;
        gbc.gridy = 20;
        gbc.weighty = 1.0;
        add(new JLabel(), gbc);
        
        JButton button = new JButton("Zavřít"); 
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
               parent.removePanel(panel);
            }
        });
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.SOUTHEAST;
        gbc.gridx = 0;
        gbc.gridwidth = 21;
        gbc.gridy = 21;
        add(button, gbc);
    }
    
    /**
     * Returns photoId of clicked button in miniatures list
     *
     * @param e button event
     */
    private int getClickedPhotoId(ActionEvent e){
        for (int i = 0; i < miniatures.size(); i++){
            if (e.getSource() == miniatures.get(i)){
                selectedMiniature = i;
                return pms.get(i).getId();
            }
        }
        return -1;
    }

    private void refreshMainImage() {
        remove(largeImage);
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.gridheight = 5;
        gbc.gridwidth = 5;
        gbc.fill = GridBagConstraints.NONE;
        imHeight = image1.getHeight();
        imWidth = image1.getWidth();
        newWidth = imWidth*(400/(double)imHeight);
        scaledImage = image1.getScaledInstance((int) newWidth, 400, Image.SCALE_DEFAULT);
        largeImage = new JLabel(new ImageIcon(scaledImage));
        add(largeImage, gbc);
        revalidate();
        repaint();
    }
}