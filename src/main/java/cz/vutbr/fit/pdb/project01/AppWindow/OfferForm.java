package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import cz.vutbr.fit.pdb.project01.models.BuildingDrawModel;
import cz.vutbr.fit.pdb.project01.models.LotDrawModel;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * @author      Karolína Hajná      <xhajna00@stud.fit.vutbr.cz>
 * @version     1.0                 
 * @since       2015-11-29          
 */
public class OfferForm extends JDialog{
    
    private boolean formValid = false;
    
    /**
    * Panel constraints for placing GUI components
    */
    GridBagConstraints gbc;
    /**
    * fields for entering name, area, address
    */
    JTextField nameField, areaField, addrField;
    /**
    * boxes for selecting type of offer, offer location
    */
    JComboBox typeBox, locationBox;
    /**
    * area for entering description
    */
    JTextArea descArea;
    
    private final BaseDrawModel entity;

    /**
    * Initializes window appearance
    * 
    * @param  entity
    */
    public OfferForm(final BaseDrawModel entity){ 
        this.entity = entity;
        setResizable(false);
        setSize(500, 500);
        setModal(true);
        
        if (entity instanceof BuildingDrawModel){
            setTitle("Nová nabídka domu/bytu");
        }
        else {
            setTitle("Nová nabídka pozemku");
        }
        setLayout(new GridBagLayout());
        
        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridx = 0;
        add(new JLabel("Název:"), gbc);
        
        gbc.gridy = 1;
        add(new JLabel("Typ:"), gbc);
        
        gbc.gridy = 2;
        add(new JLabel("Plocha:"), gbc);
        
        gbc.gridy = 3;
        add(new JLabel("Lokalita:"), gbc);
        
        gbc.gridy = 4;
        add(new JLabel("Adresa:"), gbc);
        
        gbc.gridy = 5;
        add(new JLabel("Popis:"), gbc);
        
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 3;
        nameField = new JTextField();
        add(nameField, gbc);
        
        gbc.gridy = 1;
        String[] types0 = {"Dům", "Byt"};
        typeBox = new JComboBox(types0);
        if (entity instanceof LotDrawModel){
            typeBox.setEnabled(false);
        }
        typeBox.setBackground(Color.WHITE);
        typeBox.setSelectedIndex(0);
        add(typeBox, gbc);
        
        gbc.gridy = 2;
        areaField = new JTextField();
        add(areaField, gbc);
        if (entity instanceof LotDrawModel){
            areaField.setEditable(false);
        }
        
        gbc.gridy = 3;
        String districts[] = {"<Nevybráno>", "Brno - sever", "Brno - střed", "Černovice", "Jundrov", "Komín", "Královo Pole",
            "Maloměřice a Obřany", "Medlánky", "Nový Lískovec", "Vinohrady", "Žabovřesky", "Židenice"};
        locationBox = new JComboBox(districts);
        locationBox.setBackground(Color.WHITE);
        typeBox.setSelectedIndex(0);
        add(locationBox, gbc);
     
        gbc.gridy = 4;
        addrField = new JTextField();
        add(addrField, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.gridwidth = 4;
        descArea = new JTextArea();
        descArea.setRows(12);
        JScrollPane scroll = new JScrollPane(descArea);
        add(scroll, gbc);
        
        JButton button = new JButton("OK");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) 
            {
                validateForm();
                
                if(!formValid)
                    return;
                
                if(entity instanceof BuildingDrawModel)
                {
                    BuildingDrawModel e = (BuildingDrawModel) entity;
                
                    e.setName(nameField.getText());

                    if(typeBox.getSelectedIndex() == 0)
                        e.setType("house");
                    else
                        e.setType("flat");

                    e.setArea(Integer.parseInt(areaField.getText()));
                    e.setLocality(locationBox.getSelectedIndex());
                    e.setAddress(addrField.getText());
                    e.setDescription(descArea.getText());
                }
                else
                {
                    LotDrawModel e = (LotDrawModel) entity;
                
                    e.setName(nameField.getText());
                    e.setLocality(locationBox.getSelectedIndex());
                    e.setAddress(addrField.getText());
                    e.setDescription(descArea.getText());
                }
                
                
                
                dispose();
            }
        });
        gbc.gridx = 2;
        gbc.gridy = 21;
        gbc.gridwidth = 1;
        add(button, gbc);
        
        button = new JButton("Zrušit");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        gbc.gridx = 3;
        add(button, gbc);
    }
    
    /**
     * Validate form
     */
    public void validateForm()
    {
        if("".equals(nameField.getText()))
        {
            nameField.requestFocus();
            JOptionPane.showMessageDialog(this,
            "Hodnota názvu nesmí být prázdná",
            "Chyba",
            JOptionPane.ERROR_MESSAGE);
        }
        else if("".equals(areaField.getText()) && entity instanceof BuildingDrawModel)
        {
            areaField.requestFocus();
            JOptionPane.showMessageDialog(this,
            "Hodnota plochy nesmí být prázdná",
            "Chyba",
            JOptionPane.ERROR_MESSAGE);
        }
        else if(!isNumeric(areaField.getText()) && entity instanceof BuildingDrawModel)
        {
            areaField.requestFocus();
            JOptionPane.showMessageDialog(this,
            "Hodnota plochy nesmí musí být číslo",
            "Chyba",
            JOptionPane.ERROR_MESSAGE);
        }
        else if(locationBox.getSelectedIndex() == 0)
        {
            locationBox.requestFocus();
            JOptionPane.showMessageDialog(this,
            "Lokalita musí být vybrána",
            "Chyba",
            JOptionPane.ERROR_MESSAGE);
        }
        else if("".equals(addrField.getText()))
        {
            addrField.requestFocus();
            JOptionPane.showMessageDialog(this,
            "Hodnota adresy nesmí být prázdná",
            "Chyba",
            JOptionPane.ERROR_MESSAGE);
        }
        else if("".equals(descArea.getText()))
        {
            descArea.requestFocus();
            JOptionPane.showMessageDialog(this,
            "Hodnota popisu nesmí být prázdná",
            "Chyba",
            JOptionPane.ERROR_MESSAGE);
        }
        else
            formValid = true;
    }
    
    /**
     * If the form is valid
     * @return 
     */
    public boolean isFormValid()
    {
        return formValid;
    }
    
    /**
     * Is numeric
     * @param str
     * @return 
     */
    private static boolean isNumeric(String str)  
    {  
      try  
      {  
        double d = Integer.parseInt(str);  
      }  
      catch(NumberFormatException nfe)  
      {  
        return false;  
      }  
      return true;  
    }

    void setDefaultValues(String name, String type, Integer area, Integer locality, String address, String description) 
    {
        nameField.setText(name);

        if("house".equals(type))
            typeBox.setSelectedIndex(0);
        else
            typeBox.setSelectedIndex(1);
        
        areaField.setText(Integer.toString(area));
        locationBox.setSelectedIndex(locality);
        addrField.setText(address);
        descArea.setText(description);     
    }
    
    void setDefaultValues(String name, Integer area, Integer locality, String address, String description) 
    {
        nameField.setText(name);
        areaField.setText(Integer.toString(area));
        locationBox.setSelectedIndex(locality);
        addrField.setText(address);
        descArea.setText(description);     
    }
}
