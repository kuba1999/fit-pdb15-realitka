package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.RoadDrawModel;
import java.awt.Point;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.spatial.geometry.JGeometry;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class RoadDrawRepository extends BaseDrawRepository<RoadDrawModel>{
    
    /**
     * Insert new entity
     * @param entity 
     */
    public void insert(RoadDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            //create on map
            pstmt = c.prepareStatement( "INSERT INTO " //https://docs.oracle.com/cd/B25329_01/doc/appdev.102/b28004/xe_locator.htm
                + "roads(location) values("                    
                + "SDO_GEOMETRY(2002, NULL, NULL, "
                    + "SDO_ELEM_INFO_ARRAY(1,2,1),"
                    + "SDO_ORDINATE_ARRAY("+ entity.getPointsForSQL()+")" 
                + "))", new String[]{"id"});  
        

            pstmt.execute();

            
            if(pstmt.getUpdateCount() > 0)
            {            
                try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        int ID = generatedKeys.getInt(1);
                        
                        entity.setId(ID);

                        Debugging.info("Entity ID is: " + Integer.toString(ID));                        
                    }
                    else {
                        Debugging.error("Creating entity failed, not MAP_ID obtained.");
                    }
                } 
            }
            

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with creating record in CITY_MAP");
        }
              
    }
    
    /**
     * Save new points of line to DB
     * @param entity 
     */
    public void updatePosition(RoadDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            JGeometry j_geom=new JGeometry(entity.getX(), entity.getY(), 0);
            pstmt = c.prepareStatement("UPDATE roads a SET a.location.SDO_ORDINATES = MDSYS.SDO_ORDINATE_ARRAY("+ entity.getPointsForSQL()+") where id = ?");  
            
            pstmt.setInt(1, entity.getId());


            if(pstmt.executeUpdate() > 0)
            {
                Debugging.info("Entity was updated - position changed");
            } 

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating position in CITY_MAP");
        }         
    }

    /**
     * Get all entites
     * @return 
     */
    public List<RoadDrawModel> getAll()
    {
        List<RoadDrawModel> entities = new ArrayList<>();
        
            try {
                Statement stmt = c.createStatement();
                ResultSet rset = stmt.executeQuery("SELECT id, location FROM roads");
                           


                while (rset.next()) 
                {
                    //get location
                    JGeometry jgeom = JGeometry.loadJS((Struct)rset.getObject(2) );

                    RoadDrawModel entity;

                    if(jgeom.getOrdinatesArray().length < 4) //but road has to have minimal two points => 2 * X,Y => 4
                        continue;

                    //Create object of entity
                    entity = new RoadDrawModel(new Point((int)jgeom.getOrdinatesArray()[0], (int)jgeom.getOrdinatesArray()[1]));

                    for(int i = 2; i < jgeom.getOrdinatesArray().length; i += 2)
                    {
                        entity.addPoint(new Point((int)jgeom.getOrdinatesArray()[i], (int)jgeom.getOrdinatesArray()[i+1]));
                    }         


                    entity.setId(rset.getInt(1));   

                    //add entity to list
                    entities.add(entity);
                }
     
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(SchoolDrawRepository.class.getName()).log(Level.SEVERE, null, ex);
            }        
            
            
        return entities;
    }
    
    

    /**
     * Remove entity
     * @param entity 
     */
    @Override
    public void remove(RoadDrawModel entity) 
    {
        Statement stmt;

        try {
            stmt = c.createStatement();               
            

            stmt.executeUpdate("DELETE FROM roads WHERE id = " + entity.getId());

            Debugging.info("Entity was removed");

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with removing entity");
        }                
    }    
    

}
