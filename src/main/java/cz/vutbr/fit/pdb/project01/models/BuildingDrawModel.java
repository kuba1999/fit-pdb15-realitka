/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.models;

import java.awt.Color;
import java.awt.Point;

/**
 *
 * @author Jakub Švestka
 */
public class BuildingDrawModel extends PointDrawModel implements IOffer{
    
    private String name;
    private String type;
    private Integer area = 0;
    private Integer locality;
    private String address;
    private String description;
    private String user;

    /**
     * Constructor
     * @param point 
     */
    public BuildingDrawModel(Point point) 
    {
        super(point);
        
        setFillColor(Color.ORANGE);
        setStrokeColor(Color.BLACK);
    }
    
    /**
     * Get name
     * @return 
     */
    public String getName()
    {
        return name;
    }   
    
    /**
     * Set name
     * @param name 
     */
    public void setName(String name)
    {
        this.name = name;
    }     

    /**
     * Get type
     * @return 
     */
    public String getType() 
    {
        return type;
    }

    /**
     * Set type
     * @param type 
     */
    public void setType(String type) 
    {
        this.type = type;
    }

    /**
     * Get area
     * @return 
     */
    public Integer getArea() {
        return area;
    }

    /**
     * Set area
     * @param area 
     */
    public void setArea(Integer area) {
        this.area = area;
    }

    /**
     * Get locality
     * @return 
     */
    public Integer getLocality() {
        return locality;
    }

    /**
     * Set locality
     * @param locality 
     */
    public void setLocality(Integer locality) {
        this.locality = locality;
    }

    /**
     * Geet address
     * @return 
     */
    public String getAddress() {
        return address;
    }

    /**
     * Set address
     * @param address 
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Get description
     * @return 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set description
     * @param description 
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    
    /**
     * Get user
     * @return 
     */
    public String getUser() {
        return user;
    }

    /**
     * Set user
     * @param user 
     */
    public void setUser(String user) {
        this.user = user;
    }       
}
