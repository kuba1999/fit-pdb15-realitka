/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import cz.vutbr.fit.pdb.project01.models.PhotoModel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author      Karolína Hajná      <xhajna00@stud.fit.vutbr.cz>
 * @version     1.0                 
 * @since       2015-11-30          
 */
public class EditImagePanel extends JPanel{
    /**
    * List holding clickable miniatures of gallery images
    */
    List<JButton> miniatures = new ArrayList<JButton>();
    /**
    * constraints for placing GUI components
    */
    GridBagConstraints gbc;
    /**
    * dialog holding this tab, used for closing dialog
    */
    JDialog parent;
    BaseDrawModel entity;
    /**
    * full width and height of selected image
    */
    int imHeight, imWidth;
    /**
    * recalculated width of selected image to keep width:height ratio
    */
    double newWidth;
    /**
    * objects holding original selected image, image with applied changes, miniaturized image
    */
    BufferedImage mainImage, editedImage, mini;
    /**
    * image scaled to calculated widht and height
    */
    Image scaledImage;
    /**
    * Label with scaled image
    */
    JLabel largeImage;
    /**
    * grid coordinates used for adding all gallery miniatures
    */
    int gridx, gridy;
    List<PhotoModel> pms;
    int selectedMiniature;
    
    /**
     * Initializes tab appearance
     * 
     * @param p dialog containing this tab
     * @param e
     * @throws java.io.IOException
     */
    public EditImagePanel(JDialog p, BaseDrawModel e) throws IOException{
        parent = p;
        gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridwidth = 4;
        gbc.gridheight = 7;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.gridx = 0;

        selectedMiniature = 0;
        pms = PhotoModel.findByOfferId(e.getId());
        if (!pms.isEmpty()) {
            mainImage = pms.get(selectedMiniature).getPhoto();
            imHeight = mainImage.getHeight();
            imWidth = mainImage.getWidth();
            newWidth = imWidth*(400/(double)imHeight);
            scaledImage = mainImage.getScaledInstance((int) newWidth, 400, Image.SCALE_DEFAULT);
            largeImage = new JLabel(new ImageIcon(scaledImage));
            add(largeImage, gbc);
        }
        
        gbc.gridx = 4;
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        JButton button = new JButton("Otočit doleva");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                pms.get(selectedMiniature).rotateCCW();
                editedImage = pms.get(selectedMiniature).getPhoto();
                remove(largeImage);
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.gridheight = 7;
                imHeight = editedImage.getHeight();
                imWidth = editedImage.getWidth();
                newWidth = imWidth*(400/(double)imHeight);
                scaledImage = editedImage.getScaledInstance((int) newWidth, 400, Image.SCALE_DEFAULT);
                largeImage = new JLabel(new ImageIcon(scaledImage));
                add(largeImage, gbc);
                revalidate();
                repaint();
            }
        });
        add(button, gbc);
        
        gbc.gridy = 1;
        button = new JButton("Otočit doprava");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                pms.get(selectedMiniature).rotateCW();
                editedImage = pms.get(selectedMiniature).getPhoto();
                remove(largeImage);
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.gridheight = 7;
                imHeight = editedImage.getHeight();
                imWidth = editedImage.getWidth();
                newWidth = imWidth*(400/(double)imHeight);
                scaledImage = editedImage.getScaledInstance((int) newWidth, 400, Image.SCALE_DEFAULT);
                largeImage = new JLabel(new ImageIcon(scaledImage));
                add(largeImage, gbc);
                revalidate();
                repaint();
            }
        });
        add(button, gbc);
        
        gbc.gridy = 2;
        button = new JButton("Zrcadlit horizontálně");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                pms.get(selectedMiniature).mirrorH();
                editedImage = pms.get(selectedMiniature).getPhoto();
                remove(largeImage);
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.gridheight = 7;
                imHeight = editedImage.getHeight();
                imWidth = editedImage.getWidth();
                newWidth = imWidth*(400/(double)imHeight);
                scaledImage = editedImage.getScaledInstance((int) newWidth, 400, Image.SCALE_DEFAULT);
                largeImage = new JLabel(new ImageIcon(scaledImage));
                add(largeImage, gbc);
                revalidate();
                repaint();
            }
        });
        add(button, gbc);
        
        gbc.gridy = 3;
        button = new JButton("Zdrcadlit vertikálně");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                pms.get(selectedMiniature).mirrorV();
                editedImage = pms.get(selectedMiniature).getPhoto();
                remove(largeImage);
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.gridheight = 7;
                imHeight = editedImage.getHeight();
                imWidth = editedImage.getWidth();
                newWidth = imWidth*(400/(double)imHeight);
                scaledImage = editedImage.getScaledInstance((int) newWidth, 400, Image.SCALE_DEFAULT);
                largeImage = new JLabel(new ImageIcon(scaledImage));
                add(largeImage, gbc);
                revalidate();
                repaint();
            }
        });
        add(button, gbc);
        
        /*gbc.gridy = 5;
        button = new JButton("Zrušit");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                remove(largeImage);
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.gridheight = 7;
                imHeight = mainImage.getHeight();
                imWidth = mainImage.getWidth();
                newWidth = imWidth*(400/(double)imHeight);
                scaledImage = mainImage.getScaledInstance((int) newWidth, 400, Image.SCALE_DEFAULT);
                largeImage = new JLabel(new ImageIcon(scaledImage));
                add(largeImage, gbc);
                revalidate();
                repaint();
                editedImage = deepCopy(mainImage);
            }
        });
        add(button, gbc);*/
        
        gbc.gridy = 6;
        button = new JButton("Zavřít");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                parent.dispose();
            }
        });
        add(button, gbc);
        
        gridy = 7;
        gridx = 0;
        gbc.gridwidth = 1;
        int photos_count = pms.size();
        for (int i = 0; i < photos_count; i++){
            addMiniature(pms.get(i).getId());
            if ((i + 1) % 5 != 0){
                gridx++;
            }
            else{
                gridx = 0;
                gridy++;
            }
        }
        
        gbc.gridx = 1000;
        gbc.gridy = 1000;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        add(new JLabel(), gbc);
    }

    /**
     * Copies image into new object
     * 
     * @param bi image to be copied
     */
    private static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(bi.getRaster().createCompatibleWritableRaster());
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }
    
    /**
     * Adds clickable image miniature into panel gallery
     */
    private void addMiniature(int photoId) throws IOException{
        gbc.gridx = gridx;
        gbc.gridy = gridy;
        gbc.weightx = 1.0;
        mini = PhotoModel.findById(photoId).getMini();
        imHeight = mini.getHeight();
        imWidth = mini.getWidth();
        newWidth = imWidth*(80/(double)imHeight);
        scaledImage = mini.getScaledInstance((int) newWidth, 80, Image.SCALE_DEFAULT);
        JButton miniBtn = new JButton(new ImageIcon(scaledImage));
        miniBtn.addActionListener(new ActionListener() {
            @Override
            // Setting miniature as large image
            public void actionPerformed(ActionEvent ae) {
                remove(largeImage);
                mainImage = PhotoModel.findById(getClickedPhotoId(ae)).getPhoto();
                gbc.gridwidth = 4;
                gbc.gridheight = 7;
                gbc.weightx = 0.0;
                gbc.weighty = 0.0;
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.fill = GridBagConstraints.NONE;
                imHeight = mainImage.getHeight();
                imWidth = mainImage.getWidth();
                newWidth = imWidth*(400/(double)imHeight);
                scaledImage = mainImage.getScaledInstance((int) newWidth, 400, Image.SCALE_DEFAULT);
                largeImage = new JLabel(new ImageIcon(scaledImage));
                add(largeImage, gbc);
                revalidate();
                repaint();
                editedImage = deepCopy(mainImage);
            }
        });
        miniatures.add(miniBtn);
        miniBtn.setContentAreaFilled(false);
        add(miniBtn, gbc);
    }
    
    /**
     * Returns photoId of clicked button in miniatures list
     * 
     * @param e button event
     */
    private int getClickedPhotoId(ActionEvent e){
        for (int i = 0; i < miniatures.size(); i++){
            if (e.getSource() == miniatures.get(i)){
                selectedMiniature = i;
                return pms.get(i).getId();
            }
        }
        return -1;
    }
}
