/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import cz.vutbr.fit.pdb.project01.models.PhotoModel;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author      Karolína Hajná      <xhajna00@stud.fit.vutbr.cz>
 * @version     1.0                 
 * @since       2015-11-30          
 */
public class AddImagePanel extends JPanel {
    
    /**
    * Panel constraints for placing GUI components
    */
    GridBagConstraints gbc;
    /**
    * y grid value for placing next row for adding image
    */
    int gridy = 1;
    /**
    * List of text fields holding paths to new images
    */
    List<JTextField> fields = new ArrayList<JTextField>();
    /**
    * index used for placing image path text fields
    */
    int index = 0;
    /**
    * This panel, used for removing panel tab in main window
    */
    AddImagePanel dialog = this;
    /**
    * List of buttons for browsing directory tree and setting paths to images
    */
    List<JButton> fcBtnL = new ArrayList<JButton>();
    /**
    * Controls buttons - add one more image, save, cancel selection and close dialog
    */
    JButton addNextBtn, okBtn, CancelBtn;
    /**
    * y position of button for adding new row for image selection
    */
    int addNextInd = 5;
    /**
    * dialog holding this panel, used for removing panel from tabs
    */
    JDialog parent;
    BaseDrawModel entity;
    
    /**
    * Initializes window appearance
    * 
    * @param p Dialog object holding this tab
    * @param e
    */
    public AddImagePanel(JDialog p, BaseDrawModel e) {
        parent = p;
        entity = e;
        setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.gridwidth = 4;
        gbc.gridheight = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 0.0;
        gbc.gridx = 0;  
        JLabel label = new JLabel("Úvodní fotografie");
        label.setFont(new Font(null, Font.BOLD, 22));
        add(label, gbc);
        
        addRow();
        gridy++;
        
        gbc.gridwidth = 4;
        gbc.gridx = 0;  
        gbc.gridy = gridy;
        label = new JLabel("Fotografie galerie");
        label.setFont(new Font(null, Font.BOLD, 22));
        add(label, gbc);
        gridy++;
        
        addRow();
        
        gbc.gridwidth = 1;
        gbc.gridx = 3;
        gbc.gridy = addNextInd;
        addNextBtn = new JButton("Přidat další");
        addNextBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                remove(addNextBtn);
                remove(okBtn);
                remove(CancelBtn);
                addRow();
                gbc.gridx = 3;
                gbc.gridy = ++addNextInd;
                add(addNextBtn, gbc);
                gbc.gridy = addNextInd + 1;
                add(okBtn, gbc);
                gbc.gridy = addNextInd + 2;
                add(CancelBtn, gbc);
                revalidate();
                repaint();
            }
        });
        add(addNextBtn, gbc);
        
        gbc.gridy = addNextInd + 1;
        okBtn = new JButton("OK");
        okBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                for (JTextField tf : fields) {
                    if (tf.getText().length() == 0) {
                        continue;
                    }
                    File file = new File(tf.getText());
                    if (! file.exists()) {
                        Debugging.error(AddImagePanel.class.getName() + ": file not exists.");
                        return;
                    }

                    PhotoModel pm = new PhotoModel();
                    try {
                        pm.setPhoto(ImageIO.read(file));
                    } catch (FileNotFoundException ex) {
                        Debugging.error(AddImagePanel.class.getName() + ": file not found.");
                    } catch (IOException ex) {
                        Logger.getLogger(AddImagePanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    pm.setMiniWidth(200);
                    pm.setMiniHeight(200);
                    pm.setOfferId(entity.getId());
                    pm.save();
                }
                parent.dispose();
            }
        });
        add(okBtn, gbc);
        
        gbc.gridy = addNextInd + 2;
        CancelBtn = new JButton("Zrušit");
        CancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                parent.dispose();
            }
        });
        add(CancelBtn, gbc);
        
        gbc.gridy = 1000;
        gbc.weighty = 1.0;
        add(new JLabel(""), gbc);
    }
    
    /**
    * Adds one new row for selecting image path
    */
    private void addRow(){
        gbc.weighty = 0.0;
        gbc.gridy = gridy;
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        add(new JLabel("Cesta k souboru"), gbc);
        gbc.weightx = 5.0;
        gbc.gridx = 1;
        gbc.gridwidth = 2;
        fields.add(new JTextField());
        add(fields.get(index), gbc);
        index++;
        
        gbc.gridx = 3;
        gbc.weightx = 1.0;
        gbc.gridwidth = 1;
        JButton button = new JButton("Procházet...");
        fcBtnL.add(button);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JFileChooser fc = new JFileChooser();
                if (fc.showOpenDialog(dialog) == JFileChooser.APPROVE_OPTION){
                    int row = getRow(ae);
                    fields.get(row).setText(fc.getSelectedFile().getAbsolutePath());
                }
            }
        });
        add(button, gbc);
        gridy++;
    }
    
    /**
     * gets index of button for image selection which was clicked on 
     * 
     * @param e button event
     * @return index of button in list, when not found, returns -1
     */
    private int getRow(ActionEvent e){
        for (int i = 0; i < fcBtnL.size(); i++){
            if (e.getSource() == fcBtnL.get(i)){
                return i;
            }
        }
        return -1;
    }
}
