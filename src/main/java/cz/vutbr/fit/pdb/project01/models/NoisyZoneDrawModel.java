package cz.vutbr.fit.pdb.project01.models;

import java.awt.Color;
import java.awt.Point;

/**
 *
 * @author Jakub Švestka
 */
public class NoisyZoneDrawModel extends PolygonDrawModel {
    
    /**
     * Value of noise
     */
    private Integer noise = 0;
    
    /**
     * Constructor
     * @param point 
     */
    public NoisyZoneDrawModel(Point point)
    {
        super(point);
        
        setStrokeColor(Color.RED);
    }  
    
    
    /**
     * Set value of noisy
     * @param noise 
     */
    public void setNoise(Integer noise)
    {
        this.noise = noise;
    }
    
    /**
     * Get noise
     * @return 
     */
    public Integer getNoise()
    {
        return this.noise;
    }
}
