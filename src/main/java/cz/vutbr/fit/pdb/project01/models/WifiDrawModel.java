package cz.vutbr.fit.pdb.project01.models;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author brian
 */
public class WifiDrawModel extends BaseDrawModel {
    
    /**
     * Radius
     */
    private int radius;
    
    /**
     * SSID
     */
    private String ssid;
    
    /**
     * Channel
     */
    private Integer channel;

    /**
     * Constructor
     * @param point
     * @param radius
     * @param ssid
     * @param channel
     */
    public WifiDrawModel (Point point, Integer radius, String ssid, Integer channel)
    {
        super(point);  
        
        setSSID(ssid);
        setChannel(channel);
        setRadius(radius);        

        int dx = (int)point.getX() - getX();
        int dy = (int)point.getY() - getY();
        
        setPosition(getX() + dx, getY() + dy);
        
        shape = new Ellipse2D.Double(
                getX() - radius,
                getY() - radius,
                Math.abs(getX()+(radius)*2-getX()),
                Math.abs(getY()+(radius)*2-getY())); 
        
        
        setStrokeColor(Color.BLACK);
        setFillColor(Color.YELLOW);
    } 
    
    /**
     * Constructor
     * @param point
     * @param radius
     */
    public WifiDrawModel (Point point, Integer radius)
    {
        this(point, radius, null, null);
    } 

    /**
     * Moving with entity
     * @param point 
     */
    @Override
    public void move(Point point) 
    {
        setPosition(point);
        
        int dx = (int)point.getX() - getX();
        int dy = (int)point.getY() - getY();
        
        setPosition(getX() + dx, getY() + dy);
        
        shape = new Ellipse2D.Double(
                getX() - radius,
                getY() - radius,
                Math.abs(getX()+(radius)*2-getX()),
                Math.abs(getY()+(radius)*2-getY())); 
    }   
    
    /**
     * Calculate ant set size
     * @param point 
     */
    public void calculateAndSetSize(Point point)
    {
        int dx = (int)point.getX() - getX();
        int dy = (int)point.getY() - getY();
        
        radius = (int)Math.sqrt( ( Math.pow(dx, 2) + Math.pow(dy, 2) ));
        
        if(radius < 20)
            radius = 20;
        
        
        shape = new Ellipse2D.Double(
                getX() - radius,
                getY() - radius,
                Math.abs(getX()+(radius)*2-getX()),
                Math.abs(getY()+(radius)*2-getY()));   
    }
    
    /**
     * Get ssid
     * @return 
     */
    public String getSSID()
    {
        if(ssid == null)
            return "unknown";
        
        return ssid;
    }   
    
    /**
     * Set ssid
     * @param ssid 
     */
    public void setSSID(String ssid)
    {
        this.ssid = ssid;
    }

    /**
     * Get channel
     * @return 
     */
    public Integer getChannel() 
    {
        if(channel == null)
            return 0;
        
        return channel;
    }

    /**
     * Set channel
     * @param channel 
     */
    public void setChannel(Integer channel) 
    {
        this.channel = channel;
    }

    /**
     * Get radius
     * @return 
     */
    public int getRadius() 
    {
        return radius;
    }

    /**
     * Set radius
     * @param radius 
     */
    public void setRadius(int radius) 
    {
        this.radius = radius;
    }
    
    
    
    
}
