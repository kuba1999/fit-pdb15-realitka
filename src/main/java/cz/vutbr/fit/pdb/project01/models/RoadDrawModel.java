package cz.vutbr.fit.pdb.project01.models;

import cz.vutbr.fit.pdb.project01.Debugging;
import java.awt.Color;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class RoadDrawModel extends BaseDrawModel{

    /**
     * Fake point
     */
    protected Point fakePoint = null;
    
    /**
     * Clicked point
     */
    protected Point clickedPoint = null;
    
    /**
     * Constructor
     * @param point 
     */
    public RoadDrawModel (Point point)
    {
        super(point);   

        shape = new GeneralPath();
        
        
        
        //Set starting point
        ((Path2D)shape).moveTo (point.getX(), point.getY());
        
        
        shape.contains(new Point(0, 0));
        
        setStrokeColor(Color.BLUE);
        
        
        Debugging.info("Start ponit of road was created");
    }  
    
    /**
     * Add next point
     * @param point 
     */
    public void addPoint(Point point)
    {
        removeFakePoint(); //reset
        
        ((Path2D)shape).lineTo (point.getX(), point.getY());
        
        Debugging.info("Added next ponit to road");
    }
    
    /**
     * Add fake point
     * @param point 
     */
    public void addFakePoint(Point point)
    {
        fakePoint = point;
    }   
    
    /**
     * Remove fake point
     */
    public void removeFakePoint()
    {
        fakePoint = null;
    }    
 
    /**
     * Moving with entity
     * @param point 
     */
    @Override
    public void move(Point point) 
    {
        if(clickedPoint == null)
        {
            clickedPoint = point; //first click
            return;
        }
        
        Shape newShape = new GeneralPath();
        
        double dx = (clickedPoint.getX() - point.getX()); //difference between first and second click
        double dy = (clickedPoint.getY() - point.getY()); //difference between first and second click
        
        boolean first = true;
        for(Point p: getPoints())
        {
            if(first)
            {
                setPosition((int)(p.getX() - dx), (int)(p.getY() - dy));
                
                ((Path2D)newShape).moveTo(p.getX() - dx, p.getY() - dy);
                first = false;
            }
            else
                ((Path2D)newShape).lineTo(p.getX() - dx, p.getY() - dy);
        }
        
        clickedPoint = point; //use second point as first
        
        shape = newShape; //actualize shape
    }   
    
    
    /**
     * Get shape of entity
     * @return 
     */   
    public Shape getShape()
    {
        if(fakePoint == null)
        {
            return super.getShape();
        }
        else
        {
            GeneralPath path = new GeneralPath();
            path.moveTo(getX(), getY());
            
            PathIterator pi = shape.getPathIterator(null);
            
            float[] returnedValues = new float[6];

            while (pi.isDone() == false) 
            {
                pi.currentSegment(returnedValues);
                path.lineTo(returnedValues[0], returnedValues[1]);
                pi.next();
            }
            
            //add fake point
            path.lineTo(fakePoint.getX(), fakePoint.getY());
            
            return path;
        }
    }    
    
    /**
     * Count points of line
     * @return 
     */
    public int countPoints()
    {
        int countPoints = 0;
        PathIterator pi = shape.getPathIterator(null);
        
        while (pi.isDone() == false) 
        {
            countPoints++;
            pi.next();
        }      
        
        return countPoints;
    }
    
        
    /**
     * Get points for inserting to DB
     * @return 
     */
    public String getPointsForSQL()
    {
        String points = new String();
        PathIterator pi = shape.getPathIterator(null);
        
        float[] returnedValues = new float[6];
        
        while (pi.isDone() == false) 
        {
            pi.currentSegment(returnedValues);
            
            points += Integer.toString((int)returnedValues[0]) + ", " + Integer.toString((int)returnedValues[1]);
            
            pi.next();
            
            if(pi.isDone() == false)
                points += ",";
        }   
        
        return points;
    }
    
    /**
     * Get points of line
     * @return 
     */
    public Point[] getPoints()
    {
        Point[] points = new Point[countPoints()];
        PathIterator pi = shape.getPathIterator(null);
        
        float[] returnedValues = new float[6];
        
        int i = 0;
        while (pi.isDone() == false) 
        {
            pi.currentSegment(returnedValues);
            
            points[i++] = new Point((int)returnedValues[0], (int)returnedValues[1]);
            
            pi.next();
        }   
        
        return points;
    }    
    
    /**
     * Detection If given point is part of line
     * @param point
     * @return 
     */
    public boolean containsPoint(Point point)
    {
        PathIterator pi = shape.getPathIterator(null);
        
        float[] returnedValuesStart = new float[6];
        float[] returnedValuesEnd = new float[6];
        
        boolean firstCycle = true;
        while (pi.isDone() == false) 
        {
            if(firstCycle)
            {
                pi.currentSegment(returnedValuesStart); //Start

                if(pi.isDone() == true)
                    return false;

                pi.next();
                pi.currentSegment(returnedValuesEnd); //End      
                
                firstCycle = false;
            }
            else
            {
                returnedValuesStart = returnedValuesEnd; //Start
                
                returnedValuesEnd = new float[6];

                pi.currentSegment(returnedValuesEnd); //End                
            }


            double difference = Line2D.ptSegDist(returnedValuesStart[0], returnedValuesStart[1], returnedValuesEnd[0], returnedValuesEnd[1],  point.getX(),  point.getY());
            
            //Tolerance 2.0
            if(difference < 2.0 && difference > -2.0)
                return true;
            
            pi.next();
        }   
        
        return false;
    }

    /**
     * Line is hit If contains point
     * @param point
     * @return 
     */
    public boolean isHit(Point point) 
    {
        return containsPoint(point);
    }
    
    /**
     * Removing fake point
     */
    public void clearClickedPoint()
    {
        clickedPoint = null;
    }
    
    
}