/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 * @author      Karolína Hajná      <xhajna00@stud.fit.vutbr.cz>
 * @version     1.0                 
 * @since       2015-10-26          
 */
public class ImagesDialog extends JDialog {
    
    /**
    * panel containing tabs
    */
    JTabbedPane tabsPanel;
    /**
    * Panel constraints for placing GUI components
    */
    GridBagConstraints gbc;
    
     /**
     * Initializes window appearance
     * 
     * @param entity
     * @throws java.io.IOException
     */    
    public ImagesDialog(BaseDrawModel entity) throws IOException{
        setSize(950, 700);
        setModal(true);
        
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new GridBagLayout());
        getContentPane().add(topPanel);
        
        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridwidth = 4;
        gbc.gridheight = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridx = 0;
        tabsPanel = new JTabbedPane();
        topPanel.add(tabsPanel, gbc);
        
        AddImagePanel imgPanel = new AddImagePanel(this, entity);
        JPanel topImgPanel = new JPanel(new BorderLayout());
        JScrollPane scrollPane = new JScrollPane(imgPanel);
        topImgPanel.add(scrollPane);
        tabsPanel.addTab("Nahrát fotografie", topImgPanel);
        
        EditImagePanel editPanel = new EditImagePanel(this, entity);
        JPanel topEditPanel = new JPanel(new BorderLayout());
        scrollPane = new JScrollPane(editPanel);
        topEditPanel.add(scrollPane);
        tabsPanel.addTab("Editovat fotografie", topEditPanel);
    }
    
    
}
