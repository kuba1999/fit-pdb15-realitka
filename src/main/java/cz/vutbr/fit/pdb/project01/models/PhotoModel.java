package cz.vutbr.fit.pdb.project01.models;

import cz.vutbr.fit.pdb.project01.repository.PhotoRepository;
import cz.vutbr.fit.pdb.project01.repository.PhotoRepository.ImageProcessing;
import java.awt.image.BufferedImage;
import java.util.List;

/**
 * Photo model
 * @author Ondrej Koblizek
 */
public class PhotoModel {
    private int id;
    private BufferedImage photo;
    private int miniWidth = 10;
    private int miniHeight = 10;
    private BufferedImage mini;
    private String title;
    private int offerId = 0;

    private static PhotoRepository photoRepo;

    /**
     * Main constructor
     */
    public PhotoModel() {
        photoRepo = new PhotoRepository();
    }

    /**
     * For internal usage
     * @param id
     * @param photo
     * @param miniWidth
     * @param miniHeight
     * @param mini
     * @param title
     * @param offerId
     */
    public PhotoModel(int id, BufferedImage photo, int miniWidth, int miniHeight, BufferedImage mini, String title, int offerId) {
        photoRepo = new PhotoRepository();

        this.id = id;
        this.photo = photo;
        this.miniWidth = miniWidth;
        this.miniHeight = miniWidth;
        this.mini = mini;
        this.title = title;
        this.offerId = offerId;
    }

    /**
     * Fetch photo by id
     * @param id
     * @return PhotoModel if found in DB or null
     */
    public static PhotoModel findById(int id) {
        PhotoModel p = photoRepo.getPhotoById(id);
        if (p != null) {
            return p;
        } else {
            return null;
        }
    }

    public static List<PhotoModel> findByOfferId(int offerId) {
        if (photoRepo == null) {
            photoRepo = new PhotoRepository();
        }
        List<PhotoModel> p = photoRepo.getPhotoByOfferId(offerId);
        return p;
    }

    /**
     * Store model to DB
     * @return inserted or updated record id
     */
    public int save() {
        int result;
        PhotoModel o = photoRepo.getPhotoById(this.id);
        if (o != null) {
            // update
            result = photoRepo.updatePhoto(this, ImageProcessing.NONE);
            //Debugging.warning("PhotoModel: save: update: " + result);
        } else {
            // insert
            result = photoRepo.addPhoto(this);
            //Debugging.warning("PhotoModel: save: insert: " + result);
            this.setId(result);
        }
        return result;
    }

    /**
     * Delete model from DB
     * @return true if successful
     */
    public boolean delete() {
        if (photoRepo.removePhoto(this.id) > 0) {
            this.id = 0;
            return true;
        } else {
            return false;
        }
    }

    public static int findByPhoto(BufferedImage queryImg) {
        if (photoRepo == null) {
            photoRepo = new PhotoRepository();
        }
        PhotoModel queryModel = new PhotoModel();
        queryModel.setPhoto(queryImg);
        queryModel.save();
        int queryPhotoId = queryModel.getId();

        int mostSimilar = photoRepo.searchByPhoto(queryPhotoId);
        queryModel.delete();
        return mostSimilar;
    }

    // getters
    public int getId() {
        return this.id;
    }

    public BufferedImage getPhoto() {
        return this.photo;
    }

    public BufferedImage getMini() {
        return this.mini;
    }

    public int getMiniWidth() {
        return this.miniWidth;
    }

    public int getMiniHeight() {
        return this.miniHeight;
    }

    public String getTitle() {
        return this.title;
    }

    public int getOfferId() {
        return this.offerId;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }

    public void setPhoto(BufferedImage photo) {
        this.photo = photo;
    }

    public void setMiniWidth(int miniWidth) {
        this.miniWidth = miniWidth;
    }

    public void setMiniHeight(int miniHeight) {
        this.miniHeight = miniHeight;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int rotateCW() {
        int result = -1;
        PhotoModel o = photoRepo.getPhotoById(this.id);
        if (o != null) {
            result = photoRepo.updatePhoto(this, ImageProcessing.ROTATE_CW);
            o = photoRepo.getPhotoById(this.id);
            this.photo = o.getPhoto();
            this.mini = o.getMini();
        }
        return result;
    }

    public int rotateCCW() {
        int result = -1;
        PhotoModel o = photoRepo.getPhotoById(this.id);
        if (o != null) {
            result = photoRepo.updatePhoto(this, ImageProcessing.ROTATE_CCW);
            o = photoRepo.getPhotoById(this.id);
            this.photo = o.getPhoto();
            this.mini = o.getMini();
        }
        return result;
    }

    public int mirrorH() {
        int result = -1;
        PhotoModel o = photoRepo.getPhotoById(this.id);
        if (o != null) {
            result = photoRepo.updatePhoto(this, ImageProcessing.MIRROR_H);
            o = photoRepo.getPhotoById(this.id);
            this.photo = o.getPhoto();
            this.mini = o.getMini();
        }
        return result;
    }

    public int mirrorV() {
        int result = -1;
        PhotoModel o = photoRepo.getPhotoById(this.id);
        if (o != null) {
            result = photoRepo.updatePhoto(this, ImageProcessing.MIRROR_V);
            o = photoRepo.getPhotoById(this.id);
            this.photo = o.getPhoto();
            this.mini = o.getMini();
        }
        return result;
    }

    @Override
    public String toString() {
        String out = "";
        out += "             id: " + this.id + "\n";
        out += "      miniWidth: " + this.miniWidth + "\n";
        out += "     miniHeight: " + this.miniHeight + "\n";
        out += "          title: " + this.title + "\n";
        out += "        offerId: " + this.offerId;
        return out;
    }
}
