/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class Debugging {

    protected static final String ANSI_RED = "\u001B[31m";
    protected static final String ANSI_GREEN = "\u001B[32m";
    protected static final String ANSI_YELLOW = "\u001B[33m"; 
    protected static final String ANSI_RESET = "\u001B[0m";
    protected static final String ANSI_BLUE = "\u001b[34m";
    
    /**
     * Chyba
     * @param message 
     */
    public static void error(String message)
    {
        System.err.println(ANSI_RED + "Error: " + ANSI_RESET + message);
    }   
    
    /**
     * Upozorneni
     * @param message 
     */
    public static void warning(String message)
    {
        System.err.println(ANSI_YELLOW + "Warning: " + ANSI_RESET + message);
    }  
    
    /**
     * Uspech
     * @param message 
     */
    public static void success(String message)
    {
        System.err.println(ANSI_GREEN + "Success: " + ANSI_RESET + message);
    }
    
    /**
     * Informace
     * @param message 
     */
    public static void info(String message)
    {
        System.err.println(ANSI_BLUE + "Info: " + ANSI_RESET + message);
    }
}
