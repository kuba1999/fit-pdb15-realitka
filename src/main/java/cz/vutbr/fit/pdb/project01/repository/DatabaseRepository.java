/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.models.DatabaseModel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class DatabaseRepository {

    /**
     * Connection string
     */
    private static String DEFAULT_CONNECTION_URL = "jdbc:oracle:thin:@gort.fit.vutbr.cz:1521:dbgort";
    
    /**
     * Settings
     */
    public DatabaseModel DatabaseModel;
    
    private static DatabaseRepository DatabaseRepository = null;
    
    /**
     * Constructor
     */
    public DatabaseRepository() 
    {
        DatabaseModel = new DatabaseModel();
    }
    
    /**
     * Get actuall configuration
     * @return 
     */
    public static DatabaseRepository GetConfiguration() 
    {
        if(DatabaseRepository == null)
        {
            DatabaseRepository = new DatabaseRepository();
        }
        else
        {
            return DatabaseRepository;
        }
        
        Properties prop = new Properties();
        InputStream input = null;
        
	try {
		input = new FileInputStream("config.database");
                
		// load a properties file
		prop.load(input);

		// set the properties value
                DatabaseRepository.DatabaseModel.setURL(prop.getProperty("url", DEFAULT_CONNECTION_URL));
                DatabaseRepository.DatabaseModel.setUsername(prop.getProperty("username"));
                DatabaseRepository.DatabaseModel.setPassword(prop.getProperty("password"));
		
	} catch (IOException io) {
		io.printStackTrace();
	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}    
        
        return DatabaseRepository;
    }
    
    /**
     * Save configuration
     */
    public static void SaveConfiguration()
    {
        if(DatabaseRepository == null)
        {
            return;
        }        
        
        Properties prop = new Properties();
	OutputStream output = null;

	try {
		output = new FileOutputStream("config.database");

		// set the properties value
		prop.setProperty("url", DatabaseRepository.DatabaseModel.getURL());
		prop.setProperty("username", DatabaseRepository.DatabaseModel.getUsername());
		prop.setProperty("password", DatabaseRepository.DatabaseModel.getPassword());

		// save properties to project root folder
		prop.store(output, null);

	} catch (IOException io) {
		io.printStackTrace();
	} finally {
		if (output != null) {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
    }
    
}
