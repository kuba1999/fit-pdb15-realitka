package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.BuStopDrawModel;
import java.awt.Point;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.spatial.geometry.JGeometry;

/**
 *
 * @author brian
 */
public class BusStopDrawRepository extends BaseDrawRepository<BuStopDrawModel> {

    /**
     * Insert new antity
     * @param entity 
     */
    public void insert(BuStopDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            //create on map
            pstmt = c.prepareStatement( "INSERT INTO "
                + "busStops(location, name) values("                    
                + "SDO_GEOMETRY(2001, NULL, "
                    + "SDO_POINT_TYPE(?,?,NULL),"
                    + "NULL, NULL" 
                + "), ?)", new String[]{"id"});  
        

            pstmt.setString(1, Integer.toString(entity.getX()));
            pstmt.setString(2, Integer.toString(entity.getY()));
            pstmt.setString(3, entity.getName());
            
            pstmt.execute();

            
            if(pstmt.getUpdateCount() > 0)
            {            
                try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        int ID = generatedKeys.getInt(1);
                        
                        
                        Debugging.info("Entity was created");

                        entity.setId(ID);

                        Debugging.info("Entity ID is: " + Integer.toString(ID));
                                               
                    }
                    else {
                        Debugging.error("Creating entity failed, not MAP_ID obtained.");
                    }
                } 
            }
            

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with creating record in CITY_MAP");
        }
              
    }

    /**
     * Update position of entity
     * @param entity 
     */
    public void updatePosition(BuStopDrawModel entity)
    {
        PreparedStatement pstmt;
        Statement stmt;
        

        
        try {

            JGeometry j_geom=new JGeometry(entity.getX(), entity.getY(), 0);
            pstmt = c.prepareStatement("UPDATE busStops set location=? where id = ?");  
            
            pstmt.setInt(2, entity.getId());

            Object obj = JGeometry.storeJS(j_geom, c);

            //Update position
            if(obj != null){
                pstmt.setObject(1, obj);

                if(pstmt.executeUpdate() > 0)
                {
                    Debugging.info("Entity was updated");
                } 
            }
        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating position in CITY_MAP");
        }     
    }    
    
    /**
     * Update entity
     * @param entity 
     */
    public void update(BuStopDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            pstmt = c.prepareStatement("UPDATE busStops SET name = ? WHERE id = ?");
            
            pstmt.setString(1, entity.getName());
            pstmt.setInt(2, entity.getId());
            
            if (pstmt.executeUpdate() > 0)
            {  
                Debugging.info("Entity was updated");
            }

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating entity");
        }     
    } 
    
    /**
     * Get all records
     * @return 
     */
    public List<BuStopDrawModel> getAll()
    {
        List<BuStopDrawModel> entities = new ArrayList<>();
        String map_location_id;
        
            try {
                Statement stmt = c.createStatement();
                ResultSet rset = stmt.executeQuery("SELECT id, name, location FROM busStops");
                           


                while (rset.next()) 
                {
                    //get location
                    JGeometry jgeom = JGeometry.loadJS( (Struct)rset.getObject(3) );

                    //Create object of entity
                    BuStopDrawModel entity = new BuStopDrawModel(new Point((int)jgeom.getPoint()[0],(int)jgeom.getPoint()[1]), rset.getString(2));
                    entity.setId(rset.getInt(1));

                    //add entity to list
                    entities.add(entity);
                }
     
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(BusStopDrawRepository.class.getName()).log(Level.SEVERE, null, ex);
            }        
            
            
        return entities;
    }
    
    

    /**
     * Remove entity
     * @param entity 
     */
    @Override
    public void remove(BuStopDrawModel entity) 
    {
        Statement stmt;

        try {
            stmt = c.createStatement();               
            

            stmt.executeUpdate("DELETE FROM busStops WHERE id = " + entity.getId());

            Debugging.info("Entity was removed");

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with removing entity");
        }              
    }    
}
