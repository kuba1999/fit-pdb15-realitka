package cz.vutbr.fit.pdb.project01.authenticationWindow;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Jakub Švestka
 */
public class InitDatabaseListener implements ItemListener {    

    @Override
    public void itemStateChanged(ItemEvent e) 
    {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Databáze bude inicializována do výchozího stavu", "Upozornění", JOptionPane.YES_NO_OPTION);
        
            if(dialogResult == JOptionPane.YES_OPTION)
            {
                JCheckBox ch = (JCheckBox) e.getSource();
                ch.removeItemListener(this);
                ch.setSelected(true);
                ch.addItemListener(this);
            }
        }
        
    }
}
