/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import cz.vutbr.fit.pdb.project01.models.BuStopDrawModel;
import cz.vutbr.fit.pdb.project01.models.BuildingDrawModel;
import cz.vutbr.fit.pdb.project01.models.IOffer;
import cz.vutbr.fit.pdb.project01.models.LotDrawModel;
import cz.vutbr.fit.pdb.project01.models.NoisyZoneDrawModel;
import cz.vutbr.fit.pdb.project01.models.PointDrawModel;
import cz.vutbr.fit.pdb.project01.models.RoadDrawModel;
import cz.vutbr.fit.pdb.project01.models.SchoolDrawModel;
import cz.vutbr.fit.pdb.project01.models.WifiDrawModel;
import cz.vutbr.fit.pdb.project01.repository.BaseDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.BuildingsDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.BusStopDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.LotDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.NoisyZoneDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.RoadDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.SchoolDrawRepository;
import cz.vutbr.fit.pdb.project01.repository.WifiDrawRepository;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class BaseDrawArea extends JPanel{

    /**
     * List of entities on map
     */
    protected List<BaseDrawModel> entities = new ArrayList<>();

    /** Dimension of map */
    Dimension MapDimension;
    
    /**
     * Selected entity
     */
    protected BaseDrawModel selectedEntity = null;
    
    /**
     * Actually drawed entity
     */
    protected BaseDrawModel drawedEntity = null; 
    
    /**
     * Parent of panel
     */
    protected EditorMap editorMap = null;
    
    /**
     * Constructor
     */
    public BaseDrawArea() 
    {
        Image map = new ImageIcon("img/brno_all.jpg").getImage();
        MapDimension = new Dimension(map.getWidth(null), map.getHeight(null));
        
        setPreferredSize(MapDimension);        
    }
    


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g); 
        
        Graphics2D g2D =(Graphics2D)g;
        
        Image map = new ImageIcon("img/brno_all.jpg").getImage();
        g2D.drawImage(map, 0, 0, (int)MapDimension.getWidth(), (int)MapDimension.getHeight(), this); 
        
        RenderingHints rhints = g2D.getRenderingHints();
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        

        //Paint entities
        for (BaseDrawModel entity : entities) 
        {         
            g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1));
            
            
            if(shouldBeshowed(entity) == false)
                continue;
            
            
            if(entity instanceof RoadDrawModel || entity instanceof NoisyZoneDrawModel)
            {
                g2D.setStroke(new BasicStroke(2));
                g2D.setColor(entity.getStrokeColor());
                g2D.draw( entity.getShape() );
                continue;
            }  
            
            
            if(entity instanceof LotDrawModel)
            {
                g2D.setStroke(new BasicStroke(2));
                g2D.setColor(entity.getStrokeColor());
                g2D.draw( entity.getShape() );
                
                g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.2)); 
                g2D.setColor(entity.getFillColor());
                g2D.fill(entity.getShape());                
                continue;
            }            
            
   
            
            g2D.setStroke(new BasicStroke(2));
            g2D.setColor(entity.getStrokeColor());
            g2D.draw( entity.getShape() );
            
            g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.2));            
            
            g2D.setColor(entity.getFillColor());
            g2D.fill(entity.getShape());
            
            
            if(entity instanceof SchoolDrawModel)
            {
                g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.9)); 
                Image icon = new ImageIcon("img/schoolIcon.png").getImage();
                g2D.drawImage(icon, entity.getX()-8, entity.getY()-8, 16, 16, this);                
            }
            else if(entity instanceof BuStopDrawModel)
            {
                g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.9)); 
                Image icon = new ImageIcon("img/busIcon.png").getImage();
                g2D.drawImage(icon, entity.getX()-7, entity.getY()-7, 16, 16, this);                
            }
            else if(entity instanceof WifiDrawModel && drawedEntity != entity)
            {
                g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.9)); 
                Image icon = new ImageIcon("img/wifiIcon.png").getImage();
                g2D.drawImage(icon, entity.getX()-7, entity.getY()-7, 16, 16, this);
                
                //SSID and channel
                WifiDrawModel wifi = (WifiDrawModel) entity;
                g2D.setColor(Color.black);
                g2D.drawString(wifi.getSSID() + "  [" + wifi.getChannel() + "]", entity.getX()-20, entity.getY()-8);
            }
            else if(entity instanceof BuildingDrawModel)
            {
                g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.9)); 
                
                Image icon;
                if("flat".equals(((IOffer)entity).getType()))
                    icon = new ImageIcon("img/flatIcon.png").getImage();
                else
                    icon = new ImageIcon("img/houseIcon.png").getImage();
                
                g2D.drawImage(icon, entity.getX()-7, entity.getY()-7, 16, 16, this);                
            }
        }         

    }
    
    /**
     * Load entites from DB
     */
    protected void loadEntitiesFromDB()
    {
        BaseDrawRepository rep;

        //Load schools
        rep = new SchoolDrawRepository(); 
        entities.addAll(rep.getAll());
        
        //Load bus stops
        rep = new BusStopDrawRepository(); 
        entities.addAll(rep.getAll());
        
        //Load WiFi
        rep = new WifiDrawRepository(); 
        entities.addAll(rep.getAll());
        
        //Load roads
        rep = new RoadDrawRepository(); 
        entities.addAll(rep.getAll());
        
        //Load noisy zones
        rep = new NoisyZoneDrawRepository(); 
        entities.addAll(rep.getAll());
    }
    
    /**
     * Load offers from DB
     */
    protected void loadOffersFromDB()
    {
        BaseDrawRepository rep;

        //Load buildings
        rep = new BuildingsDrawRepository(); 
        entities.addAll(rep.getAll());
        
        //Load lots
        rep = new LotDrawRepository(); 
        entities.addAll(rep.getAll());
    }
    
    
    /**
     * Get hit entity
     * @param position
     * @return BaseDrawModel | null
     */
    protected BaseDrawModel getHitEntity(Point position)
    {
        List<BaseDrawModel> hitEntities = new ArrayList<>();

        //select existing entity       
        for (BaseDrawModel entity : entities) 
        {
            if(entity.isHit(position) && shouldBeshowed(entity))
            {
                hitEntities.add(entity);
            }              
        }
        
        if(hitEntities.size() == 1)
            return hitEntities.get(0);
        
        for (BaseDrawModel entity : hitEntities) 
        {
            if(entity instanceof PointDrawModel)  
                return entity;
        }
        
        if(hitEntities.size() > 1)
            return hitEntities.get(0);        
        
        return null;
    }    
    
    private boolean shouldBeshowed(BaseDrawModel entity)
    {
        if(editorMap == null)
            return true;
        
        
        if((entity instanceof BuildingDrawModel || entity instanceof LotDrawModel) && editorMap.offersChk.isSelected())
        {
            return true;
        }
        else if(entity instanceof WifiDrawModel && editorMap.wifiChk.isSelected())
        {
            return true;
        }
        else if(entity instanceof RoadDrawModel && editorMap.roadsChk.isSelected())
        {
            return true;
        }
        else if(entity instanceof NoisyZoneDrawModel && editorMap.noiseChk.isSelected())
        {
            return true;
        }
        else if(entity instanceof SchoolDrawModel && editorMap.schoolChk.isSelected())
        {
            return true;
        }
        else if(entity instanceof BuStopDrawModel && editorMap.busChk.isSelected())
        {
            return true;
        }
        
        
        return false;
    }
}
