package cz.vutbr.fit.pdb.project01;


import cz.vutbr.fit.pdb.project01.AppWindow.AppWindow;
import cz.vutbr.fit.pdb.project01.authenticationWindow.DatabasePanel;
import cz.vutbr.fit.pdb.project01.authenticationWindow.LoginPanel;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class EstateAgency extends JFrame{
    
    /**
     * Connection to database
     */
    private Database db;
    private AppWindow app;

    /**
     * Constructor
     */
    public EstateAgency() {
        
        super("Realitní kancelář");
        
        setSize(300, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Method called after closing main window
        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                exitApplication();
            }
        }); 
        
        setSize(500, 250);   
        setResizable(false);
        
        //Components       
        JTabbedPane jtp = new JTabbedPane();
        jtp.addTab("Přihlášení", new LoginPanel(this));
        jtp.addTab("Nastavení databáze", new DatabasePanel());
 
        add(jtp);
    }
    
    /**
     * Method is called after successful connection to DB
     * @param db 
     * @param loggedUser 
     */
    public void connectionToDatabaseSuccess(Database db, String loggedUser) 
    {
        this.db = db;
        
        this.setVisible(false);
        try {
            app = new AppWindow(loggedUser);
        } catch (IOException ex) {
            Logger.getLogger(EstateAgency.class.getName()).log(Level.SEVERE, null, ex);
        }
        app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                setVisible(true);
            }
        }); 
        app.setVisible(true);
    }    


    /**
     * Main
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {
        // execute tests (please uncomment for testing)
        //new PhotoTest(); // Photo model.
        // end of tests
        EstateAgency main = new EstateAgency();
        
        main.setVisible(true);
    }    

    /**
     * Close connection to DB after shutdown
     */
    protected void exitApplication() 
    {
        try {
            if(db != null && db.isConnected())
            {
                db.disconnect();
                Debugging.success("Odpojen od DB");
            }
        } catch (SQLException ex) {
            Debugging.error("Problem pri odpojovani od DB: " + ex.getMessage());
        }

        setVisible(false);
        dispose();
        
        Debugging.info("Aplikace ukoncena");
    }
}
