/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.models;

/**
 *
 * @author Jakub Švestka
 */
public class DatabaseModel {
    
    private String URL;
    private String username;
    private String password;

    /**
     * @return String
     */
    public String getURL() {
        return URL;
    }

    /**
     * @param URL 
     */
    public void setURL(String URL) {
        this.URL = URL;
    }

    /**
     * @return String
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username 
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return String
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password 
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
