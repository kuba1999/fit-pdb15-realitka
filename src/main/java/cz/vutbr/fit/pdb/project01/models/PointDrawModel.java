package cz.vutbr.fit.pdb.project01.models;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author Jakub Švestka
 */
public class PointDrawModel extends BaseDrawModel{
    
    private static final int WIDTH = 25;
    private static final int HEIGHT = 25;  

    /**
     *
     * @param point
     */
    public PointDrawModel (Point point)
    {
        super(point);   

        shape = new Ellipse2D.Double(
                getX() - PointDrawModel.WIDTH/2,
                getY() - PointDrawModel.HEIGHT/2, 
                PointDrawModel.WIDTH, 
                PointDrawModel.HEIGHT);
        
        setStrokeColor(Color.BLACK);    
    }  

    @Override
    public void move(Point point) 
    {
        setPosition(point);
        
        shape = new Ellipse2D.Double(
                getX() - PointDrawModel.WIDTH/2,
                getY() - PointDrawModel.HEIGHT/2, 
                PointDrawModel.WIDTH, 
                PointDrawModel.HEIGHT);
    }
}
