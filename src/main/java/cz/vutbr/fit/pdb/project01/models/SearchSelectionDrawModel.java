package cz.vutbr.fit.pdb.project01.models;

import java.awt.Point;
import java.awt.Polygon;

/**
 *
 * @author Jakub Švestka
 */
public class SearchSelectionDrawModel extends PolygonDrawModel{

    /**
     * Constructor
     * @param point 
     */
    public SearchSelectionDrawModel(Point point) {
        super(point);
    }
    
    
    /**
     * Remove last point
     */
    public void removeLastPoint()
    {
        Point[] points = new Point[countPoints()];

        
        Polygon polygon = (Polygon) shape;
        Polygon newPolygon = new Polygon();

        int j = 0;
        for(int i=0; i< (polygon.npoints-1); i++)
        {
            newPolygon.addPoint(polygon.xpoints[i], polygon.ypoints[i]);
        }
        
        shape = newPolygon;
    }     
}
