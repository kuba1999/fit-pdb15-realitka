/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;

import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.BaseDrawModel;
import cz.vutbr.fit.pdb.project01.models.SearchSelectionDrawModel;
import cz.vutbr.fit.pdb.project01.repository.SearchOffersRepository;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public class SearchDrawArea extends BaseDrawArea{
    
    private SearchPanel searchPanel;
    
    private SearchSelectionDrawModel selection = null;
    private SearchSelectionDrawModel selected = null;
    private Date showFromDate = new Date();

   /**
     * Constructor
     * 
     * @param searchPanel
     */
    public SearchDrawArea(SearchPanel searchPanel) 
    {
        this.searchPanel = searchPanel;
        
        
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                try {
                    entityMouseClicked(evt);
                } catch (IOException ex) {
                    Logger.getLogger(EditDrawArea.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            public void mouseEntered(MouseEvent evt) {
                //formMouseEntered(evt);
            }
            public void mouseExited(MouseEvent evt) {
                //formMouseExited(evt);
            }
            public void mousePressed(MouseEvent evt) {
                
            }
            public void mouseReleased(MouseEvent evt) {
            }
        });

        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) 
            {
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) 
            {
                entityMouseMoved(evt);
            }
        });               
        
        //load entities
        loadEntitiesFromDB();
        
        //bind
        bindSearchEvent();
    }
    
    private void bindSearchEvent()
    {
        searchPanel.searchBtn.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
              getResults();
          }
        });
        
        searchPanel.timeAxis.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) 
            {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MONTH, ((JSlider) ce.getSource()).getValue()-60);
                showFromDate = cal.getTime();
                
                
                System.out.println(showFromDate);
            }
        });
        
        searchPanel.backBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                if(selection != null)
                {
                    selection.removeLastPoint();
                    
                    if(selection.countPoints() <= 1) //remove
                    {
                        entities.remove(selection);
                        selection = null;
                        searchPanel.polygonBtn.setSelected(false);
                    }
                    
                    repaint();
                }
            }
        });
        
        searchPanel.clearBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) 
            { 
                entities.remove(selection);
                entities.remove(selected);
                
                selection = null;
                selected = null;        
                
                searchPanel.polygonBtn.setSelected(false);
                
                repaint();
            }
        });
    }
    
    private void getResults()
    {
        //remove all
        entities = new ArrayList<>();
        
        //load entities
        loadEntitiesFromDB();        
        
        
        SearchOffersRepository rep = new SearchOffersRepository();
        
        //searchByPhoto
        if(!"".equals(searchPanel.searchImageField.getText()))
        {
            rep.byPhoto(searchPanel.searchImageField.getText());
        }

        //type
        if(searchPanel.typeCB.getSelectedIndex() == 1)
            rep.onlyBuildings();
        else if(searchPanel.typeCB.getSelectedIndex() == 2)
            rep.onlyLots();
        
        //Location
        if(searchPanel.districtsL.getSelectedIndices().length > 0)
        {
            rep.inLocalities(searchPanel.districtsL.getSelectedIndices());
        }
        
        //Wifi
        if(searchPanel.wifiCB.getSelectedIndex() == 1)
        {
            rep.inWifiAP();
        }
        else if(searchPanel.wifiCB.getSelectedIndex() == 2)
        {
            rep.notInWifiAP();
        }
        
        //Noisy zones
        if(searchPanel.noiseCB.getSelectedIndex() == 1)
        {
            rep.inNoise();
        }
        else if(searchPanel.noiseCB.getSelectedIndex() == 2)
        {
            rep.notInNoise();
        }        
        
        //School distance
        if(!"".equals(searchPanel.schoolFld1.getText()) && !"".equals(searchPanel.schoolFld2.getText()))
        {
            rep.schoolDistance(searchPanel.schoolFld1.getText(), searchPanel.schoolFld2.getText());
        }
        
        //Road distance
        if(!"".equals(searchPanel.roadFld1.getText()) && !"".equals(searchPanel.roadFld2.getText()))
        {
            rep.roadDistance(searchPanel.roadFld1.getText(), searchPanel.roadFld2.getText());
        }
        
        //Bus distance
        if(!"".equals(searchPanel.mhdFld1.getText()) && !"".equals(searchPanel.mhdFld2.getText()))
        {
            rep.busDistance(searchPanel.mhdFld1.getText(), searchPanel.mhdFld2.getText());
        }
        
        //Offers distance
        if(!"".equals(searchPanel.existsField.getText()))
        {
            rep.existsOfferDistance(searchPanel.existsField.getText());
        }          
        
        //Date
        rep.setFromDate(showFromDate);
        
        
        
        //Search in selected area
        if(selected != null)
        {
            rep.inSelectedArea(selected);
            entities.add(selected);
        }        
        
        List<BaseDrawModel> result = rep.search();
        
        entities.addAll(result);
        
        //remove old results panel
        searchPanel.getMainFrame().removePanelWithResults();
        
        if(result.isEmpty() == false)
        {
            ResultsPanel rp = searchPanel.getMainFrame().showPanelWithResults();

            for(BaseDrawModel e: result)
            {
                rp.addResult(e);
            }

            rp.addFiller();            
        }
        
        JOptionPane.showMessageDialog(searchPanel.getMainFrame(),
        "Počet výsledků: " + result.size(),
        "Výsledek vyhledávání",
        JOptionPane.INFORMATION_MESSAGE);
        
        repaint();
    }
    
    
    
    /**
     * Actions when mouse clicked
     * @param position 
     */
    private void entityMouseClicked(MouseEvent evt) throws IOException
    {     
        
        if(evt.getButton() == 3 && selection != null) //end of drawing
        {
            selection.removeFakePoint();

            repaint();

            //Save to DB
            if(selection.countPoints() > 2) //Insert line to DB if we have segment of line
            {
                Debugging.info("We can search");
                selected = selection;
                selection = null;
                searchPanel.polygonBtn.setSelected(false);
            }
            else
            {
                entities.remove(selection);
                selection = null;
                searchPanel.polygonBtn.setSelected(false);
            }

            return;
        }
        
        if(selected != null)
            return;
        
        if(searchPanel.polygonBtn.isSelected() == false)
            return;
        

        //add new point to drawen entity
        if(selection != null)
        {
            Debugging.info("next point");

            //add point
            selection.addPoint(evt.getPoint());
        }
        else
        {
            SearchSelectionDrawModel entity = new SearchSelectionDrawModel(evt.getPoint());
            
            entity.setFillColor(Color.BLACK);
            entity.setStrokeColor(Color.BLACK);

            selection = entity;  

            entities.add(entity);
        }

        repaint();
       
    }
    
    private void entityMouseMoved(MouseEvent evt)
    {
        if(selection != null)
        {
            selection.addFakePoint(evt.getPoint());
            repaint();
        }
    }
    
}
