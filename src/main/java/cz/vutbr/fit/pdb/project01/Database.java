/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01;

import cz.vutbr.fit.pdb.project01.models.DatabaseModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.regex.Pattern;
import oracle.jdbc.pool.OracleDataSource;

/**
 *
 * @author jakub
 */
public class Database {
    
    /**
     * Connection
     */
    private Connection conn = null;
    
    private static Database database = null;
    
    private static DatabaseModel databaseModel = null;
    
    /**
     * Connect to DB
     * @return boolean
     */
    private boolean connect() throws SQLException
    {
        if(databaseModel == null)
            return false;
        
        OracleDataSource ods = new OracleDataSource();
        ods.setURL(databaseModel.getURL());
        ods.setUser(databaseModel.getUsername());
        ods.setPassword(databaseModel.getPassword());
        
        //Save connection
        conn = ods.getConnection();
        
        //Povoleni auto commitu
        conn.setAutoCommit(true);
        
        Debugging.success("Pripojen k DB");
        
        return true;
    }
    
    /**
     * Disconnect from DB
     * @throws SQLException 
     */
    public void disconnect() throws SQLException
    {
        if(isConnected())
        {
            conn.close();
        }
    }
    
    /**
     * @return boolean
     */
    public boolean isConnected() {
        if(conn != null) {
            try{
                if(conn.isValid(0)) {
                    return true;
                }
                else {
                    return false;
                }
            } catch (SQLException e) {
                return false;
            }
        }
        
        return false;
    }
    
    /**
     * Get database
     * @return Database
     */
    public static Database getDatabase()
    {
        if(database == null)
        {
            database = new Database();
            try {
                if(!database.connect())
                    return null;
            } catch (SQLException ex) {
                Debugging.error("Problem pri pripojovani k DB: " + ex.getMessage());
            }
        }
        
        if(database.isConnected() == false)
        {
            try {
                if(!database.connect())
                    return null;
            } catch (SQLException ex) {
                Debugging.error("Problem pri pripojovani k DB: " + ex.getMessage());
            }
        }
        
        return database;
    }
    
    /**
     * Get database
     * @param model
     * @return Database
     */
    public static Database getDatabase(DatabaseModel model)
    {
        setDatabaseModel(model);
        
        return getDatabase();
    }
    
    /**
     * Get connection
     * @return 
     */
    public static Connection getConnection()
    {
        return getDatabase().conn;
    }
    
    /**
     * Set database model
     * @param model 
     */
    public static void setDatabaseModel(DatabaseModel model)
    {
        databaseModel = model;
    }
    
    
    /**
     * Initialization a database
     */
    public void initDatabase()
    {
        executeSqlScript(";", "tableStructure.sql");
        executeSqlScript(";", "initDB.sql");
    }
    
    /**
     * Execute script from SQL file
     * @param delimeter
     * @param filename 
     */
    private void executeSqlScript(String delimeter, String filename)
    {
        try {        
            String content = new Scanner(new File(getPathToSqlFiles() + filename)).useDelimiter("\\Z").next();
        
            content = removeComments(content);
            
            String[] commandsArray = getCommands(content, delimeter);
            for (String command : commandsArray) {
                executeCommand(command);
                
                Debugging.info(command);
            }
        
        } catch (FileNotFoundException ex) {
            Debugging.error("Soubor s SQL prikazy nebyl nalezen");
        }
                
    }
    
    private void executeCommand(String command)
    {
        try {
            Statement stmt = Database.getConnection().createStatement();
            stmt.executeQuery(command);
        } catch (SQLException ex) {
            Debugging.error("Problem pri vykonavani prikazu: " + command + ". Error code:" + ex.getErrorCode() + ", Msg: " + ex.getMessage());
        }       
    }
    
    private String[] getCommands(String content, String delimeter)
    {
        //explode text by delimeter
        String[] data = content.split(delimeter);
        
        return data;
    }
    
    /**
     * Remove comments from text
     * @param content
     * @return 
     */
    private String removeComments(String content)
    {
        String withoutComments;

        //remove multiline comments
        withoutComments = Pattern.compile("\\/\\*(.*)\\*\\/", Pattern.DOTALL).matcher(content).replaceAll("");
        //remove single line comments
        withoutComments = Pattern.compile("--(.*)").matcher(withoutComments).replaceAll(""); 
        //remove empty lines
        withoutComments = Pattern.compile("(?m)^[ \\t]*\\r?\\n").matcher(withoutComments).replaceAll(""); 
                
        return withoutComments;
    }
    
    /**
     * Returns path with SQL files
     * @return 
     */
    private String getPathToSqlFiles()
    {
       String path;
        
        try {
            path = new File("").getCanonicalPath();
        } catch (IOException ex) {
            return null;
        }   
        
        return path + "/sql/";
    }
}
