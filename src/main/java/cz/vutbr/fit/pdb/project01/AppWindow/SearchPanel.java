/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.AppWindow;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;


/**
 * @author      Karolína Hajná      <xhajna00@stud.fit.vutbr.cz>
 * @version     1.0                 
 * @since       2015-10-27         
 */
public class SearchPanel extends JPanel {
    SearchPanel dialog = this;
    /**
    * map used for selecting area for search
    */
    private SearchDrawArea drawArea;
    /**
    * buttons for clearing selected area from map, undoing last selection, searching
    */
    public JButton clearBtn, backBtn, searchBtn;
    /**
    * button for selecting polygonal area
    */
    public JToggleButton polygonBtn;
    /**
    * form text fields
    */
    public JTextField schoolFld1, schoolFld2, roadFld1, roadFld2, mhdFld1, mhdFld2, existsField, searchImageField;
    /**
    * boxes for type of offer, wifi and noise selection
    */
    public JComboBox typeCB, wifiCB, noiseCB;
    /**
    * list for offer districts selection
    */
    public JList districtsL;
    /**
    * slider for showing offers in time
    */
    public JSlider timeAxis;
    
    private AppWindow mainFrame;
    
    /**
    * Initializes tab appearance
     * @param mainFrame
    */
    public SearchPanel(AppWindow mainFrame)
    {
        this.mainFrame = mainFrame;
        
        searchBtn = new JButton("Vyhledat");
        polygonBtn = new JToggleButton("Polygon");               
        backBtn = new JButton("Zpět");
        clearBtn = new JButton("Smazat výběr");        
        timeAxis = new JSlider(JSlider.HORIZONTAL, 0, 60, 60);
        
        
        
        setLayout(new GridBagLayout());
        GridBagConstraints gridConstraints = new GridBagConstraints();
 
        gridConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridConstraints.insets = new Insets(20, 5, 5, 5);
        gridConstraints.gridwidth = 1;
        gridConstraints.gridheight = 1;
        gridConstraints.weightx = 0;
        gridConstraints.weighty = 0;
        
        JLabel label = new JLabel("Typ nemovitosti:");
        gridConstraints.insets = new Insets(5, 5, 5, 5);
        gridConstraints.gridx = 0;
        gridConstraints.gridy = 0;
        add(label, gridConstraints);
        
        label = new JLabel("Městská čtvrť:");
        gridConstraints.gridy = 1;
        add(label, gridConstraints);
        
        label = new JLabel("Vzdálenost školy od [m]:");
        gridConstraints.gridy = 4;
        add(label, gridConstraints);
        
        label = new JLabel("Vzdálenost MHD od [m]:");
        gridConstraints.gridy = 5;
        add(label, gridConstraints);
        
        label = new JLabel("Vzdálenost silnice od [m]:");
        gridConstraints.gridy = 6;
        add(label, gridConstraints);
        
        label = new JLabel("Existuje další nabídka do [m]:");
        gridConstraints.gridy = 7;
        add(label, gridConstraints);
        
        label = new JLabel("Pokrytí Wifi:");
        gridConstraints.gridy = 8;
        add(label, gridConstraints);

        label = new JLabel("Hluková zóna:");
        gridConstraints.gridy = 9;
        add(label, gridConstraints);
        
        label = new JLabel("Hledat podle fotky:");
        gridConstraints.gridy = 10;
        add(label, gridConstraints);

        String[] types = {"<Nevybráno>", "Dům / Byt", "Pozemek"};
        typeCB = new JComboBox(types);
        typeCB.setBackground(Color.WHITE);
        typeCB.setSelectedIndex(0);
        gridConstraints.gridwidth = 3;
        gridConstraints.gridx = 1;
        gridConstraints.gridy = 0;
        add(typeCB, gridConstraints);
        
        String districts[] = {"Brno - sever", "Brno - střed", "Černovice", "Jundrov", "Komín", "Královo Pole",
            "Maloměřice a Obřany", "Medlánky", "Nový Lískovec", "Vinohrady", "Žabovřesky", "Židenice"};
        gridConstraints.fill = GridBagConstraints.BOTH;
        districtsL = new JList(districts);
        districtsL.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        districtsL.setVisibleRowCount(4);
        gridConstraints.gridheight = 3;
        gridConstraints.gridwidth = 3;
        gridConstraints.gridx = 1;
        gridConstraints.gridy = 1;   
        JPanel distrPanel = new JPanel();
        distrPanel.add(new JScrollPane(districtsL), gridConstraints);
        add(distrPanel, gridConstraints);
        
        schoolFld1 = new JTextField();
        gridConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridConstraints.weightx = 3;
        gridConstraints.gridwidth = 1;
        gridConstraints.gridheight = 1;
        gridConstraints.gridy = 4;
        add(schoolFld1, gridConstraints);
        
        label = new JLabel("do [m]:");
        gridConstraints.weightx = 0;
        gridConstraints.gridx = 2;
        gridConstraints.gridy = 4;
        add(label, gridConstraints);
        
        schoolFld2 = new JTextField();
        gridConstraints.weightx = 3;
        gridConstraints.gridx = 3;
        gridConstraints.gridy = 4;
        add(schoolFld2, gridConstraints);
        
        mhdFld1 = new JTextField();
        gridConstraints.gridwidth = 1;
        gridConstraints.gridx = 1;
        gridConstraints.gridy = 5;
        add(mhdFld1, gridConstraints);
        
        label = new JLabel("do [m]:");
        gridConstraints.weightx = 0;
        gridConstraints.gridx = 2;
        gridConstraints.gridy = 5;
        add(label, gridConstraints);
        
        mhdFld2 = new JTextField();
        gridConstraints.weightx = 1;
        gridConstraints.gridx = 3;
        gridConstraints.gridy = 5;
        add(mhdFld2, gridConstraints);          
        
        roadFld1 = new JTextField();
        gridConstraints.gridwidth = 1;
        gridConstraints.gridx = 1;
        gridConstraints.gridy = 6;
        add(roadFld1, gridConstraints);
        
        label = new JLabel("do [m]:");
        gridConstraints.weightx = 0;
        gridConstraints.gridx = 2;
        gridConstraints.gridy = 6;
        add(label, gridConstraints);
        
        roadFld2 = new JTextField();
        gridConstraints.weightx = 3;
        gridConstraints.gridx = 3;
        gridConstraints.gridy = 6;
        add(roadFld2, gridConstraints);
        
        existsField = new JTextField();
        gridConstraints.gridwidth = 3;
        gridConstraints.gridx = 1;
        gridConstraints.gridy = 7;
        add(existsField, gridConstraints);        
        
        String[] wifi = {"<Nevybráno>", "Ano", "Ne"};
        wifiCB = new JComboBox(wifi);
        wifiCB.setBackground(Color.WHITE);
        wifiCB.setSelectedIndex(0);
        gridConstraints.gridx = 1;
        gridConstraints.gridy = 8;
        add(wifiCB, gridConstraints);
        
        String[] noise = {"<Nevybráno>", "Ano", "Ne"};
        noiseCB = new JComboBox(wifi);
        noiseCB.setBackground(Color.WHITE);
        noiseCB.setSelectedIndex(0);
        gridConstraints.gridwidth = 3;
        gridConstraints.gridy = 9;
        add(noiseCB, gridConstraints);

        searchImageField = new JTextField();
        gridConstraints.gridwidth = 2;
        gridConstraints.gridx = 1;
        gridConstraints.gridy = 10;
        add(searchImageField, gridConstraints);
        
        gridConstraints.gridwidth = 1;
        gridConstraints.gridx = 3;
        JButton button = new JButton("Procházet...");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JFileChooser fc = new JFileChooser();
                if (fc.showOpenDialog(dialog) == JFileChooser.APPROVE_OPTION){
                    searchImageField.setText(fc.getSelectedFile().getAbsolutePath());
                }
            }
        });
        add(button, gridConstraints);

        drawArea = new SearchDrawArea(this);
        JScrollPane scroll = new JScrollPane(drawArea);
        scroll.setAutoscrolls(true);
        gridConstraints.fill = GridBagConstraints.BOTH;
        gridConstraints.insets = new Insets(5, 5, 5, 0);
        gridConstraints.gridheight = 20;
        gridConstraints.gridwidth = 10;
        gridConstraints.gridx = 4;
        gridConstraints.gridy = 0;
        gridConstraints.weightx = 25;
        gridConstraints.weighty = 90;
        add(scroll, gridConstraints);

        gridConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridConstraints.insets = new Insets(20, 5, 5, 5);
        gridConstraints.gridheight = 1;
        gridConstraints.gridwidth = 1;
        gridConstraints.weightx = 0;
        gridConstraints.weighty = 0;
        
        Font font;
        label = new JLabel("Vybrat Oblast");
        label.setFont(font = new Font(null, Font.BOLD, 15));
        gridConstraints.gridx = 20;
        gridConstraints.gridy = 0;
        add(label, gridConstraints);

        gridConstraints.insets = new Insets(5, 5, 5, 5);
        gridConstraints.gridy = 1;
        add(polygonBtn, gridConstraints);

        JSeparator sep = new JSeparator(JSeparator.CENTER);
        gridConstraints.insets = new Insets(15, 5, 15, 5);
        gridConstraints.gridy = 2;
        add(sep, gridConstraints);
        
        label = new JLabel("Úprava výběru");
        label.setFont(new Font(font.getAttributes()));
        gridConstraints.insets = new Insets(5, 5, 5, 5);
        gridConstraints.gridy = 3;
        add(label, gridConstraints);

        gridConstraints.gridy = 4; 
        add(backBtn, gridConstraints);
        
        
        gridConstraints.gridy = 5; 
        add(clearBtn, gridConstraints); 
        
        sep = new JSeparator(JSeparator.CENTER);
        gridConstraints.insets = new Insets(15, 5, 15, 5);
        gridConstraints.gridy = 6;
        add(sep, gridConstraints);
        
        
        gridConstraints.insets = new Insets(5, 5, 5, 5);
        gridConstraints.gridy = 7; 
        add(searchBtn, gridConstraints);

        JLabel axisLabel = new JLabel("Časová osa");
        axisLabel.setFont(new Font(font.getAttributes()));
        gridConstraints.gridx = 4;
        gridConstraints.gridy = 20;
        add(axisLabel, gridConstraints);

        
        timeAxis.setMajorTickSpacing(10);
        timeAxis.setMinorTickSpacing(1);
        timeAxis.setPaintTicks(true);
        Hashtable<Integer, JLabel> labels = new Hashtable<Integer, JLabel>();
        labels.put(0, new JLabel("5 let"));
        labels.put(12, new JLabel("4 roky"));
        labels.put(24, new JLabel("3 roky"));
        labels.put(36, new JLabel("2 roky"));
        labels.put(48, new JLabel("1 rok"));
        labels.put(60, new JLabel("Nyní"));
        timeAxis.setLabelTable(labels);
        timeAxis.setPaintLabels(true);
        gridConstraints.gridx = 5;
        gridConstraints.gridy = 20;
        gridConstraints.gridwidth = 17;
        add(timeAxis, gridConstraints);
    }
    
    /**
     * Get main frame of application
     * @return 
     */
    public AppWindow getMainFrame()
    {
        return mainFrame;
    }       
}
