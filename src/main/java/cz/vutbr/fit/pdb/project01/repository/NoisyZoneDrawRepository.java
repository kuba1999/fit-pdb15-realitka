/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.NoisyZoneDrawModel;
import java.awt.Point;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.spatial.geometry.JGeometry;

/**
 *
 * @author Jakub Švestka
 */
public class NoisyZoneDrawRepository extends BaseDrawRepository<NoisyZoneDrawModel>{
    
    /**
     * Insert new entity
     * @param entity 
     */
    public void insert(NoisyZoneDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            //create on map
            pstmt = c.prepareStatement( "INSERT INTO " //https://docs.oracle.com/cd/B25329_01/doc/appdev.102/b28004/xe_locator.htm
                + "noisyZones(location, noise) values("                    
                + "SDO_GEOMETRY(2003, NULL, NULL, "
                    + "SDO_ELEM_INFO_ARRAY(1,1003,1),"
                    + "SDO_ORDINATE_ARRAY("+ entity.getPointsForSQL() +")" 
                + "), ?)", new String[]{"id"});
            
            
            pstmt.setInt(1, entity.getNoise()); //nois
        

            pstmt.execute();

            
            if(pstmt.getUpdateCount() > 0)
            {            
                try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) 
                    {
                        int ID = generatedKeys.getInt(1);

                            Debugging.info("Entity was created");
                     
                            entity.setId(generatedKeys.getInt(1));
                                    
                            Debugging.info("Entity ID is: " + Integer.toString(generatedKeys.getInt(1)));
                    }                        
                }
                catch (SQLException ex)
                {
                    Debugging.error("Creating entity failed, not ID obtained.");
                }
        
            } 
            
            

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with creating record in CITY_MAP");
        }
              
    }
    
    /**
     * Update entity
     * @param entity 
     */
    public void update(NoisyZoneDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            pstmt = c.prepareStatement("UPDATE noisyZones SET noise = ? WHERE id = ?");
            
            pstmt.setInt(1, entity.getNoise());
            pstmt.setInt(2, entity.getId());
            
            if (pstmt.executeUpdate() > 0)
            {  
                Debugging.info("Entity was updated");
            }

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating entity");
        }     
    }     
    
    /**
     * Save new points of line to DB
     * @param entity 
     */
    public void updatePosition(NoisyZoneDrawModel entity)
    {
        PreparedStatement pstmt;
        Statement stmt;
        
        try {
            stmt = c.createStatement();               
            
            
            JGeometry j_geom=new JGeometry(entity.getX(), entity.getY(), 0);
            pstmt = c.prepareStatement("UPDATE noisyZones a SET a.location.SDO_ORDINATES = MDSYS.SDO_ORDINATE_ARRAY("+ entity.getPointsForSQL()+") WHERE id = " + Integer.toString(entity.getId()));
            

            if(pstmt.executeUpdate() > 0)
            {
                Debugging.info("Entity was updated - position changed");
            } 
            

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating position in CITY_MAP");
        }         
    }

    /**
     * Get all entites
     * @return 
     */
    public List<NoisyZoneDrawModel> getAll()
    {
        List<NoisyZoneDrawModel> entities = new ArrayList<>();
        
            try {
                Statement stmt = c.createStatement();
                ResultSet rset = stmt.executeQuery("SELECT id, noise, location FROM noisyZones");

                while (rset.next()) 
                {

                    //get location
                    JGeometry jgeom = JGeometry.loadJS((Struct)rset.getObject(3) );

                    NoisyZoneDrawModel entity;

                    if(jgeom.getOrdinatesArray().length < 4) //but road has to have minimal two points => 2 * X,Y => 4
                        continue;

                    //Create object of entity
                    entity = new NoisyZoneDrawModel(new Point((int)jgeom.getOrdinatesArray()[0], (int)jgeom.getOrdinatesArray()[1]));

                    for(int i = 2; i < jgeom.getOrdinatesArray().length; i += 2)
                    {
                        entity.addPoint(new Point((int)jgeom.getOrdinatesArray()[i], (int)jgeom.getOrdinatesArray()[i+1]));
                    }         


                    entity.setId(rset.getInt(1)); 
                    entity.setNoise(rset.getInt(2)); 

                    //add entity to list
                    entities.add(entity);

                }
     
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(SchoolDrawRepository.class.getName()).log(Level.SEVERE, null, ex);
            }        
            
            
        return entities;
    }
    
    

    /**
     * Remove entity
     * @param entity 
     */
    @Override
    public void remove(NoisyZoneDrawModel entity) 
    {
        Statement stmt;

        try {
            stmt = c.createStatement();               
            

            stmt.executeUpdate("DELETE FROM noisyZones WHERE id = " + entity.getId());

            Debugging.info("Entity was removed");

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with removing entity");
        }              
    }    
    
    
}
