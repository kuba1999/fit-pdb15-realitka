
package cz.vutbr.fit.pdb.project01.models;

import java.awt.Color;
import java.awt.Point;
import java.awt.Shape;

/**
 * @author      Jakub Švestka      <xsvest05@stud.fit.vutbr.cz>         
 */
public abstract class BaseDrawModel{

    /**
     * Shape of entity
     */
    protected Shape shape;
    
    /**
     * Position
     */
    protected Point position;
    
    /**
     * Stroke color
     */
    protected Color strokeColor;
    
    /**
     * Fill color
     */
    protected Color fillColor;
    
    /**
     * Is selected
     */
    protected boolean selected = false;
    
    /**
     * Selected color
     */
    static Color selectedColor = Color.orange;
    
    /**
     * ID
     */
    protected int id;  
    
    /**
     * Constructor
     * @param point 
     */
    public BaseDrawModel(Point point) 
    {
        position = point;
    }
    
    /**
     * Get shape of entity
     * @return 
     */   
    public Shape getShape()
    {
        return shape;
    }

    
    /**
     * Get X position
     * @return double
     */    
    public int getX()
    {
        if(position == null)
            return 0;
        
        return (int)position.getX();
    } 
    
    /**
     * Get Y position
     * @return double
     */
    public int getY()
    {
        if(position == null)
            return 0;
        
        return (int)position.getY();
    } 
    
    
    /**
     * Set position
     * @param x
     * @param y
     */    
    public void setPosition(int x, int y)
    {
        position.setLocation(x, y);
    } 
    
    /**
     * Set position
     * @param position
     */    
    public void setPosition(Point position)
    {
        this.position = position;
    } 

    /**
     * Set stroke color
     * @param color 
     */
    public void setStrokeColor(Color color)
    {
        strokeColor = color;
    } 
    
    /**
     * Get stroke color
     * @return 
     */
    public Color getStrokeColor()
    {
        if(isSelected())
            return selectedColor;
        
        return strokeColor;
    }
    
    /**
     * Set fill color
     * @param color 
     */
    public void setFillColor(Color color)
    {
        fillColor = color;
    } 
    
    /**
     * Get fill color
     * @return 
     */
    public Color getFillColor()
    {
        return fillColor;
    }
    
    /**
     * If entity is hit
     * @param point
     * @return 
     */
    public boolean isHit(Point point)
    {
        if(point != null && shape.contains(point))
            return true;
        else
            return false;
    }    
    
    /**
     * Moving with entity
     * @param point 
     */
    abstract public void move(Point point);
    
    /**
     * Is entity selected?
     * @return 
     */
    public boolean isSelected()
    {
        return selected;
    }
    
    /**
     * Set entity selected
     */
    public void setSelected()
    {
        selected = true;
    }
    
    /**
     * Set if the entity is selected
     * @param selected
     */
    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }    

    /**
     * Get ID
     * @return 
     */
    public int getId() 
    {
        return id;
    }

    /**
     * Set id
     * @param id 
     */
    public void setId(int id) 
    {
        this.id = id;
    }
    
    
}
