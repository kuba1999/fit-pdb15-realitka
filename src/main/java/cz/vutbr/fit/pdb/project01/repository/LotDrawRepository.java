/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vutbr.fit.pdb.project01.repository;

import cz.vutbr.fit.pdb.project01.Debugging;
import cz.vutbr.fit.pdb.project01.models.LotDrawModel;
import java.awt.Point;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.spatial.geometry.JGeometry;

/**
 *
 * @author Jakub Švestka
 */
public class LotDrawRepository  extends BaseDrawRepository<LotDrawModel>{
    
    /**
     * Insert new entity
     * @param entity 
     */
    public void insert(LotDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            //create on map
            pstmt = c.prepareStatement( "INSERT INTO " //https://docs.oracle.com/cd/B25329_01/doc/appdev.102/b28004/xe_locator.htm
                + "offers(location, name, locality, address, description, \"user\", building) values("                    
                + "SDO_GEOMETRY(2003, NULL, NULL, "
                    + "SDO_ELEM_INFO_ARRAY(1,1003,1),"
                    + "SDO_ORDINATE_ARRAY("+ entity.getPointsForSQL() +")" 
                + "), ?, ?, ?, ?, ?, 0)", new String[]{"id"}); 
        
            pstmt.setString(1, entity.getName());
            pstmt.setInt(2, entity.getLocality());
            pstmt.setString(3, entity.getAddress());
            pstmt.setString(4, entity.getDescription());
            pstmt.setString(5, entity.getUser());
            pstmt.execute();

            
            if(pstmt.getUpdateCount() > 0)
            {            
                try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        int ID = generatedKeys.getInt(1);
                        
                        entity.setId(ID);
                        entity.setArea(getArea(entity));

                        Debugging.info("Entity ID is: " + Integer.toString(ID));                      
                    }
                    else {
                        Debugging.error("Creating entity failed, not MAP_ID obtained.");
                    }
                } 
            }
            

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with creating record in CITY_MAP");
        }
              
    }
    
    /**
     * Update entity
     * @param entity 
     */
    public void update(LotDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            pstmt = c.prepareStatement("UPDATE offers SET name = ?, locality = ?, address = ?, description = ? WHERE id = ?");
            
            pstmt.setString(1, entity.getName());
            pstmt.setInt(2, entity.getLocality());
            pstmt.setString(3, entity.getAddress());
            pstmt.setString(4, entity.getDescription());
            pstmt.setInt(5, entity.getId());
            
            if (pstmt.executeUpdate() > 0)
            {  
                Debugging.info("Entity was updated");
            }

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating entity");
        }     
    }   
    
    /**
     * Save new points of line to DB
     * @param entity 
     */
    public void updatePosition(LotDrawModel entity)
    {
        PreparedStatement pstmt;
        
        try {
            JGeometry j_geom=new JGeometry(entity.getX(), entity.getY(), 0);
            pstmt = c.prepareStatement("UPDATE lots a SET a.location.SDO_ORDINATES = MDSYS.SDO_ORDINATE_ARRAY("+ entity.getPointsForSQL()+") where id = ?");  
            
            pstmt.setInt(1, entity.getId());


            if(pstmt.executeUpdate() > 0)
            {
                Debugging.info("Entity was updated - position changed");
            }

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with updating position in CITY_MAP");
        }         
    }
    
    /**
     * Get all entities
     * @return 
     */
   public List<LotDrawModel> getAll()
    {
        List<LotDrawModel> entities = new ArrayList<>();
        
            try {
                Statement stmt = c.createStatement();
                ResultSet rset = stmt.executeQuery("SELECT id, name, area, locality, address, description, \"user\", location, SDO_GEOM.SDO_AREA(location, 1) FROM offers WHERE building = 0 and solddate IS NULL");
                           


                while (rset.next()) 
                {
                    //get location
                    JGeometry jgeom = JGeometry.loadJS((Struct)rset.getObject(8) );

                    LotDrawModel entity;

                    if(jgeom.getOrdinatesArray().length < 4) //but road has to have minimal two points => 2 * X,Y => 4
                        continue;

                    //Create object of entity
                    entity = new LotDrawModel(new Point((int)jgeom.getOrdinatesArray()[0], (int)jgeom.getOrdinatesArray()[1]));
                    entity.setId(rset.getInt(1));  
                    entity.setName(rset.getString(2));
                    entity.setArea(rset.getInt(3));
                    entity.setLocality(rset.getInt(4));
                    entity.setAddress(rset.getString(5));
                    entity.setDescription(rset.getString(6));
                    entity.setUser(rset.getString(7));
                    entity.setArea(rset.getInt(9)*4);

                    for(int i = 2; i < jgeom.getOrdinatesArray().length; i += 2)
                    {
                        entity.addPoint(new Point((int)jgeom.getOrdinatesArray()[i], (int)jgeom.getOrdinatesArray()[i+1]));
                    }           

                    //add entity to list
                    entities.add(entity);
                }
     
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(SchoolDrawRepository.class.getName()).log(Level.SEVERE, null, ex);
            }        
            
            
        return entities;
    } 

    /**
     * Remove entity
     * @param entity 
     */
    @Override
    public void remove(LotDrawModel entity) 
    {
        Statement stmt;

        try {
            stmt = c.createStatement();               
            

            //stmt.executeUpdate("DELETE FROM offers WHERE id = " + entity.getId());
            stmt.executeUpdate("UPDATE offers SET solddate = CURRENT_TIMESTAMP WHERE id = " + entity.getId());

            Debugging.info("Entity was removed");

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with removing entity");
        }              
    }      
    
    /**
     * Get the area of lot
     * @param entity
     * @return 
     */
    public int getArea(LotDrawModel entity)
    {
        try {
            Statement stmt = c.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT SDO_GEOM.SDO_AREA(location, 1) FROM offers WHERE id = " + Integer.toString(entity.getId()));

            if (rset.next())
            {
                return rset.getInt(1)*4;
            }


        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            Debugging.error("Problem with calculating area"); 
        }  
        
        return 0;
    }
}
