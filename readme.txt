Instrukce k spusteni:

a) Stahnout zip archiv z https://drive.google.com/file/d/0B5sAspQ-C4-aRTFUQ3A5Yi1mR1E/view?usp=sharing 
b) Rozbalit stazeny xkobli00_libs_imgs.zip do adresare s projektem (obsahuje adresare imgs a libs)
c) v config.database zkontrolovat uzivatelsky login a heslo pro DB (melo by byt prednastaveno)
d) spustit projekt pomoci skriptu run.sh (spusti predkompilovany .jar ze slozky target)
